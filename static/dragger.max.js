function dragger_init(){
/*
DRAGGER!
*/


var testmode = false;

var firstPlay = true;

var canvasWidth = 1000;
var canvasHeight = 600;

var colors = {
    red : 0xEC184d,
    cream : 0xfffff0,
    green : 0xE8F7C4,
    orange: 0xEDBC6E,
    yellow: 0xFAFCA2,
    black: 0x000000,
    blue: 0x7EC3ED,
    peach: 0xF7CFBA
};

var baseColor = colors.cream;
var buildupColor = colors.yellow;
var playingColor = colors.green;


var size = [canvasWidth, canvasHeight];
var ratio = size[0] / size[1];

var socket = io();
//var renderer = PIXI.autoDetectRenderer();
var renderer = new PIXI.CanvasRenderer(canvasWidth, canvasHeight, {transparent: true});
//var renderer = new PIXI.WebGLRenderer(canvasWidth, canvasHeight, {transparent: true, antialias: true});

//renderer.backgroundColor = 0xFFFFF0;

var cssBg = jQuery('body');

var shapes = [];

document.body.appendChild(renderer.view);

var stage = new PIXI.Container();

var bgfader = new PIXI.Graphics();

var pauseCheck = null;
var rampDownCheck = null;
// 

var compIsPlaying = false;

TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});


// create a texture from an image path
var texture = PIXI.Texture.fromImage('/static/img/drag/bunny.png');
var circ = PIXI.Texture.fromImage('/static/img/drag/circ.png');
var rect = PIXI.Texture.fromImage('/static/img/drag/rect.png');
var tri = PIXI.Texture.fromImage('/static/img/drag/tri.png');
var bar = PIXI.Texture.fromImage('/static/img/drag/bar.png');

var textures = [circ,rect,tri,bar];
//console.log('textures length ' + textures.length);

var notes = [
	'D3',
	'G3',
	'C3',
	'D4',
	'G4',
	'C4',
	'D5',
	'G5'
]

var imDragging = false;
var isPlaying = false;
var fdataSort;
var hasData = false;

var bxId = 0;
    bxId = 1000;

var texNo = 0;
var noteNo = 0;

var bgVolUp = -10;
var bgVolDown = -30;

var chillData = [
	{
		id: 0,
		x: 100,
		y: 100
	},
	{
		id: 1,
		x: 100,
		y: 100
	},
	{
		id: 2,
		x: 100,
		y: 100
	},
	{
		id: 3,
		x: 100,
		y: 100
	},
	{
		id: 4,
		x: 100,
		y: 100
	},
	{
		id: 5,
		x: 100,
		y: 100
	},
	{
		id: 6,
		x: 100,
		y: 100
	},
	{
		id: 7,
		x: 100,
		y: 100
	},
];
var linesCont = new PIXI.Container();
var shapesCont = new PIXI.Container();
stage.addChild(linesCont);


var lineColors = [
    0xFF0000,
    0xFFFF00,
    0x00FF00,
    0x00FFFF,
    0xFF700B,
    0x00FFBB,
    0xF0FFF0,
    0xDDFFA7,
    0xFF00FF
];

var spriteNames = [
    'jerry'
];

var circNo = 1, barNo = 1, triNo = 1, rectNo = 1;


function createShape(x, y, i) {

    var bxId = i;

    if (noteNo < notes.length -1) {
        noteNo ++;
    } else {
        noteNo = 0;
    }


    if (texNo < textures.length -1) {
        texNo ++;
    } else {
        texNo = 0;
    }

    var shape = new PIXI.Sprite(textures[texNo]);

    shape.anchor.set(0.5);

    shape.scale.set(.5);


    // var shape = new PIXI.Graphics();


    // shape.lineStyle(0);
    // shape.beginFill(colors.red, 0.5);
    // shape.drawCircle(0,0,60,60);
    // shape.endFill();
    // shape.cacheAsBitmap = true;

    shape.lines = [];

    shape.interactive = false;

    shape.buttonMode = true;
    shape.defaultCursor = "move"; 

    if (!testmode) {
        shape.alpha = 0;
    }
    shape.shapeId = i;
    shape.note = notes[noteNo];
    shape.newNote = false;
    shape
        .on('mousedown', onDragStart)
        .on('touchstart', onDragStart)
        .on('mouseup', onDragEnd)
        .on('mouseupoutside', onDragEnd)
        .on('touchend', onDragEnd)
        .on('touchendoutside', onDragEnd)
        .on('mousemove', onDragMove)
        .on('touchmove', onDragMove);

    shape.position.x = x;
    shape.position.y = y;

    if (textures[texNo] == bar) {
        shape.rotation = 0.59009582;
        shape.funName = 'bar ' + barNo;
        barNo ++;
    } else if (textures[texNo] == circ) {
        shape.funName = 'circle ' + circNo;
        circNo ++;
    } else if (textures[texNo] == rect) {
        shape.funName = 'rectangle ' + rectNo;
        rectNo ++;
    } else if (textures[texNo] == tri) {    
        shape.funName = 'triangle ' + triNo;
        triNo ++;    
    }

    // AUDIO //
    setupAudio(shape);
    
    // add it to the stage
    shapesCont.addChild(shape);
    shapes.push(shape);
    //shape.mask = myMask
}





//shapesCont.addChild(blaSprite);
//blajam.addChild(shapesCont);
//stage.addChild(blajam);

for (var i = 0; i < 8; i++) {
    createShape(10*i+1 , 10*i+1, i);
}

stage.addChild(shapesCont);
//var blajam = new PIXI.Container();




var userMoves = 0;
function onDragEnd(event) {
    // CLENT DRAG END
    var dragData = {};

    imDragging = false;
    
    dragData.id = this.shapeId;
    dragData.x = this.position.x;
    dragData.y = this.position.y;

    keepInBounds(dragData); 
    
    this.alpha = 1;
    this.dragging = false;
    this.data = null;

    var posAll = getPositionAll();

    var dataForServer = {};
    dataForServer.dragData = dragData;
    dataForServer.posAll = posAll;

    socket.emit('drag end', dataForServer);

    TweenMax.to(cssBg, 2, {backgroundColor:buildupColor, delay:1, ease:Linear.easeNone});
 
    this.synth.triggerRelease();
    
    startPlayingTimer();
    if (hasData) {
        fdataSort.unshift(formatMsg(posAll));
    }
    userMoves++;
    if (userMoves == 1) {
        localMessage('You just made your first move', 'local');
    } else {
        localMessage('You have made '+userMoves+' moves so far', 'local');
    }
}


var tempData = false;

socket.on('update drag', function(msg) {
    if (gotInitialData) {
        // REMOTE DRAG END
        console.log(msg);
        var id = msg.dragData.id;
        var sprt = shapes[id];
        //keepInBounds(msg.dragData); 
        sprt.position.x = msg.dragData.x;
        sprt.position.y = msg.dragData.y;

        sprt.scale.set(.5);

        sprt.remoteDragger = false;    
        sprt.interactive = true;    
        sprt.alpha = 1;

        TweenMax.to(cssBg, 2, {backgroundColor:buildupColor, delay:1, ease:Linear.easeNone});
        
        setTimeout(function(){
            sprt.verb.wet.rampTo(0, .3);        
            sprt.synth.volume.rampTo(-30, .3); 
        },300);
           
        sprt.synth.triggerRelease();

        startPlayingTimer();

        fdataSort.unshift(formatMsg(msg.posAll));
    }
});


function startPlayingTimer() {
    clearPlayingTimer();
    pauseCheck = setTimeout(function() {
        playing(30, 100, false, endPlaying);
        TweenMax.to(cssBg, .3, {backgroundColor:playingColor, ease:Linear.easeNone});        
        pauseCheck = null;
    }, 3000);

    // AUDIO //

    rampDownCheck = setTimeout(function(){
        dsVol.volume.rampTo(bgVolDown, 3);
        rampDownCheck = null;
    },1200)    
}


function endPlaying() {
    TweenMax.to(cssBg, .2, {backgroundColor:baseColor, ease:Linear.easeNone, onComplete:function(){
        dsVol.volume.rampTo(bgVolUp, .4);
        compIsPlaying = false;

        if (!arpIntoduced) {
            setTimeout(function(){
                arpSyn.volume.rampTo(-23, 3); 
            },1400);
            arpIntoduced = true;
        }   
    }});    
}
function onDragStart(event) {
    var sprt = this;

    function okToDrag(sprt){
        console.log('ok to drag', sprt);
        dsVol.volume.rampTo(bgVolUp, .3);
        TweenMax.killTweensOf(cssBg);    
        TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});


        imDragging = sprt.shapeId;    
        sprt.data = event.data;
        sprt.alpha = 1;
        sprt.dragging = true;
        //sprt.defaultCursor = "grabbing"; //or some othe css cursor style
                
        socket.emit('drag start', sprt.shapeId);

        sprt.verb.wet.value = 0;
        sprt.synth.triggerAttack(sprt.note);
        clearPlayingTimer();
        localMessage('You are dragging '+sprt.funName, 'local');
    }
    // attempting to prevent mutiple users from grabbing the same shape
    setTimeout(function(){
        if (!sprt.remoteDragger) {
            okToDrag(sprt);
        }
    },200);
}



socket.on('flag drag', function(msg) {
    // REMOTE DRAG START
    console.log('Got the drag start', msg);

    var id = msg.shapeId;
    var sprt = shapes[id];
    sprt.remoteDragger = true;
    // IF someone else is dragging, turn off interactivity;
    if (imDragging != id) {
        sprt.interactive = false;
    }

    dsVol.volume.rampTo(bgVolUp, .3);
    TweenMax.killTweensOf(cssBg);
    TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});

    sprt.verb.wet.value = 1;
    sprt.synth.volume.value = -42;    
    sprt.synth.triggerAttack(sprt.note);

    sprt.alpha = .2;
    sprt.scale.set(.5);
    clearPlayingTimer();
    localMessage(msg.appUserName+' is dragging '+sprt.funName, 'remote');
});



function clearPlayingTimer() {
    clearTimeout(pauseCheck);
    clearTimeout(rampDownCheck);    
}


function onDragMove(event) {
    if (this.dragging) {

        var newPosition = this.data.getLocalPosition(this.parent);
        this.position.x = newPosition.x;
        this.position.y = newPosition.y;

        var dragData = {};
        
        dragData.id = this.shapeId;
        dragData.x = newPosition.x;
        dragData.y = newPosition.y;
        
        socket.emit('drag live', dragData);
        
        updateSound(dragData, false);
        updateDrone(dragData);
    }
}




socket.on('draging live', function(msg) {
   // console.log('draging live', msg);
    if (gotInitialData) {

        var id = msg.id;
        var sprt = shapes[id];

        var dragData = {};
        dragData.id = msg.id;
        dragData.x = msg.x;
        dragData.y = msg.y;

        sprt.position.x = msg.x;
        sprt.position.y = msg.y;

        updateSound(dragData, false);
    }        
});

function setupEffects(){

    var dsFilterArgs = {
        type:"highpass",
        //frequency:350,
        //rolloff:-12,
        Q:2,
        gain:0        
    }

    dSFilter = new Tone.Filter(dsFilterArgs);

    dsLfo = new Tone.LFO(.1, 400, 4000);
    dsLfo.connect(dSFilter.frequency);
    dsLfo.start();

    sDelay = new Tone.FeedbackDelay(.15, 0);

    droneVol = new Tone.Volume(bgVolUp);

    dsVol = new Tone.Volume(bgVolUp);



   // dsVol.chain(sDelay, Tone.Master);

    dReverb = new Tone.JCReverb(0.4);    
}
setupEffects();


var shapeSynthDefault = {
    "oscillator" : {
        "type" : "square"
    },
    "envelope" : {
        "attack" : .02,
        "decay" : 1,
        "sustain" : 1,
        "release" : 5.2,
    }
}

function setupAudio(shape) {
    shape.synth = new Tone.Synth({
        "oscillator" : {
            "type" : "square"
        },
        "envelope" : {
            "attack" : .02,
            "decay" : 1,
            "sustain" : 1,
            "release" : 5.2,
        }
    }).toMaster();    

    var filterArgs = {
        type:"lowpass",
        frequency:350,
        rolloff:-12,
        Q:10,
        gain:1        
    }

    shape.verb = new Tone.JCReverb(0.7).toMaster();

    shape.verb.wet.value = 0;

    shape.filter = new Tone.Filter(filterArgs).toMaster();

    //shape.synth.connect(shape.filter);

    shape.synth.chain(shape.filter, shape.verb);

    shape.synth.volume.value = -30;    
}

function updateSound(data, attack, intr) {
    if (!intr) {
        intr = .1;
    }

    var obj = shapes[data.id];
    var syn = obj.synth;

    var oNote = obj.note;
    var nNote = changeNote(obj.synth, data.y);

    var vel = 1;

    adjustFilter(obj.filter, data.x);
    
    syn.setNote(nNote);

    //console.log(data.id, oNote, nNote);
    if (oNote == nNote) {
        //console.log('the same note');
        syn.newNote = false;
        vel = .1;
        // syn.volume.value = -60;    

    } else {
        //console.log('DIFFERENT NOTE');
        syn.newNote = true;
       // syn.volume.value = -30;    
    }

    if (attack && data.hasMoved) {
        //console.log('TRIGGER!!');
        syn.triggerAttackRelease(nNote, intr);
    }    


    obj.note = nNote;    
}

function updateDrone(data){
    var d = data.x;
    var x = d.map(0, 1000, 0, 100);
    dReverb.set({gain: data.x});
}


function adjustFilter(filt,pos) {
    var x = pos.map(0, 1000, 0, 500)
    filt.set({frequency: x});
}


function hasNoteChanged(syn) {
    var oldNote = '';
    var newNOte = '';
    if (oldNote == newNote) {
        changed = false;
    } else {
        changed = true;
    }
    return changed;
}

function changeNote(syn, pos){
    var note;
    if (pos >= 0 && pos <= 100) {
        note = "C3"; 
    } else if (pos >= 100 && pos <= 200) {
        note = "D3"; 
    } else if (pos >= 200 && pos <= 300) {
        note = "G3"; 
    } else if (pos >= 300 && pos <= 400) {
        note = "D4"; 
    } else if (pos >= 400 && pos <= 500) {
        note = "G4"; 
    } else if (pos >= 500 && pos <= 600) {
        note = "D5"; 
    } else {
        note = "D7"; 
    } 
    return note;   
}


for (var i = 0; i < shapes.length; i++) {
    var shp = shapes[i];
    //shp.synth.connect(sDelay);
}  


//var masterDelay = new Tone.FeedbackDelay(1, 0);

//Tone.Master.connect(masterDelay);

var ds1, ds2, ds3, ds4;
var dSFilter, dsLfo, sDelay, dsVol, dReverb, droneVol;

function setupDroneSynths() {
    ds1 = new Tone.Synth({
        "oscillator" : {
            "type" : "sine"
        },
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    });    

    ds2 = new Tone.Synth({
        "oscillator" : {
            "type" : "triangle"
        },
        //"detune" : -700,
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    }); 

    ds3 = new Tone.Synth({
        "oscillator" : {
            "type" : "sine"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    }); 

    ds4 = new Tone.Synth({
        "oscillator" : {
            "type" : "square"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    }); 

}



function wireDroneSynths() {

    ds1.chain(droneVol);

    ds2.chain(droneVol);

    ds3.chain(droneVol);

    ds4.chain(dSFilter, sDelay, dReverb, droneVol);

    droneVol.connect(dsVol);

}





function playDroneSynths() {
    setupDroneSynths();
    wireDroneSynths();

    ds1.volume.value = -30; 
    ds1.triggerAttack('G2');

    ds2.volume.value = -60; 
    ds2.triggerAttack('B6');

    ds3.volume.value = -20; 
    ds3.triggerAttack('G3');

    //ds4.connect(dSFilter);
    ds4.volume.value = -45; 
    ds4.triggerAttack('D3');

}


playDroneSynths();

var arpSyn;
var arpIntoduced = false;
var notesUp = [
    'D5',
    'G6',
    'C5',
    'D6',
    'G5',
    'C6',
    'D6',
    'G4'
]

function setUpArp() {
	arpSyn = new Tone.Synth({
        "oscillator" : {
            "type" : "square"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : 1.2,
            "decay" : 1,
            "sustain" : 1,
            "release" : 1.2,
        }		
	});

	var pattern = new Tone.Pattern(function(time, note){
	    arpSyn.triggerAttackRelease(note, 0.25);
	}, notes, "random");

	pattern.start(0);
	Tone.Transport.start();
    arpSyn.volume.value = -10000; 
    arpSyn.chain(sDelay, dsVol, Tone.Master);




    arpSyn2 = new Tone.Synth({
        "oscillator" : {
            "type" : "sine"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : .1,
            "decay" : 1,
            "sustain" : 1,
            "release" : .1,
        }       
    });

    var pattern2 = new Tone.Pattern(function(time, note){
        arpSyn2.triggerAttackRelease(note, 0.05);
    }, notesUp, "random");
   // pattern2.interval = 2;
    pattern2.playbackRate = 3.3;
    pattern2.humanize = true;
    pattern2.start(.2);
    //Tone.Transport.start();
    arpSyn2.volume.value = -48; 
    arpSyn2.chain(sDelay, dsVol, Tone.Master);    
}

setUpArp();
var gotInitialData = false;
var serverSaysAnimating = false;

socket.emit('CLIENT get comp');

socket.on('SERVER send comp', function(data) {
    console.log('data from server', data.Items.length);
    // GET ALL FROM DB AND SORT
    var fdata = data.Items;
    fdataSort = fdata.sort(function(a, b) {
        return parseFloat(b.ts) - parseFloat(a.ts);
    });
    console.log(JSON.stringify(fdataSort));
    gotInitialData = true;

    var sprts = [];
    // POSITION THE COMPOSITION
    for (var i = 0; i < comp.length; i++)
    {
        var key = comp[i];
        var sprt = shapes[key.id];
        sprt.interactive = true;

        sprt.position.x = key.x;
        sprt.position.y = key.y;
        sprts.push(sprt);


        var objData = {
            id : key.id,
            x : key.x,
            y : key.y
        }
        updateSound(objData); 
        keepInBounds(objData); 
    }
    var timer = 100;
  
    setTimeout(function(){
        TweenMax.staggerTo(sprts, .4, {alpha:1},.3);
    },timer);

});

playing = function(numb, intr, sequence, callback){
    compIsPlaying = true;
    
    var curIndex;
    var xx = [];
    var aniInterval;
  
    if (intr) {
        aniInterval = intr
    } else {
        aniInterval = 100
    }

    var aniIntervalSecs = aniInterval / 1000;
    
    if (numb == 'all') {

        if (sequence) {
            curIndex = sequence.length;
        } else {
            if (hasData) {
                curIndex = fdataSort.length;
            } else {
                curIndex = 10;
            }
        }


    } else if (numb != undefined) {
        curIndex = numb;
    } else {
        curIndex = 10;
        if (testmode) {
            curIndex = 5;
        }
    }

    for (var i = 0; i < shapes.length; i++) {
        var shp = shapes[i];
        shp.interactive = false;
    }    


    
    function advanceComp() {
        --curIndex;
        if (curIndex >= 0) {
            //console.log('index', curIndex, fdataSort.length);

            /// COPY PASTE YO
            var comp;

            if (sequence) {
                comp = sequence[curIndex];
            } else {
                if (hasData) {
                    comp = fdataSort[curIndex].things;

                } else {
                    comp = chillData;
                }
            }
            xx.push(comp);
            for (var i = 0; i < comp.length; i++)
            {
                var key = comp[i];
                var sprt = shapes[key.id];
                sprt.alpha = 1;

                var hasMoved = false;
                if (key.x == sprt.position.x && key.y == sprt.position.y) {

                } else {
                    hasMoved = true;
                    var objData = {
                        id : key.id,
                        x : key.x,
                        y : key.y,
                        hasMoved : hasMoved
                    }
                    updateSound(objData, true, aniIntervalSecs);

                                       
                    // var line = new PIXI.Graphics();

                    // line.lineStyle(1, colors.red, .5);
                    // line.moveTo(sprt.position.x, sprt.position.y);
                    // line.lineTo(key.x, key.y);
                    // linesCont.addChild(line);
                    // sprt.lines.push(line);
                    // if (sprt.lines.length > 2) {
                    //     var lastLine = sprt.lines.shift();
                    //     linesCont.removeChild(lastLine);
                    // }   


                }
                TweenMax.to(sprt.position, (aniIntervalSecs), {x:key.x , y:key.y});

            }



           setTimeout(advanceComp, aniInterval);
        } else {
            firstPlay = false;
            curIndex = false;
            if (callback) {
                callback();    
            }
            var xy = JSON.stringify(xx)
            //console.log(xy);

            for (var i = 0; i < shapes.length; i++) {
                var shp = shapes[i];
                shp.interactive = true; 
            }
                
        }
    }

    advanceComp();
}

removeLines = function() {
    for (var i = shapes.length - 1; i >= 0; i--) {
        for (var ii = shapes[i].lines.length - 1; ii >= 0; ii--) {
            linesCont.removeChild(shapes[i].lines[ii]);
        }
    }    
}

tint = function(){
    for (var i = shapes.length - 1; i >= 0; i--) {
        for (var ii = shapes[i].lines.length - 1; ii >= 0; ii--) {
            shapes[i].lines[ii].tint = 0x000000;
        }
    }
}
var ssss = [
    /*
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 510.7794361525705, "y": 207.9723601813863, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 335.82089552238807, "y": 256.89483912761824, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 384.742951907131, "y": 144.95357374217232, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 384.742951907131, "y": 144.95357374217232, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 349.08789386401327, "y": 36.32908659036925, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 363.18407960199005, "y": 207.1431656229756, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 475.9535655058043, "y": 240.31094795940402, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 285.240464344942, "y": 411.12502699201036, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 312.60364842454396, "y": 365.5193262794213, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 348.2587064676617, "y": 322.40120924206434, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 373.13432835820896, "y": 290.06262146404663, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 411.27694859038144, "y": 224.55625134960053, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 421.22719734660035, "y": 173.14618872813648, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 402.1558872305141, "y": 198.02202548045778, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 391.3764510779436, "y": 222.06866767436838, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 174.12935323383084, "y": 222.8978622327791, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 619.4029850746268, "y": 206.3139710645649, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 391.3764510779436, "y": 222.06866767436838, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 391.3764510779436, "y": 222.06866767436838, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 736.318407960199, "y": 342.3018786439214, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 545.6053067993366, "y": 351.42301878643923, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 399.66832504145935, "y": 488.24012092420645, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 223.88059701492537, "y": 468.3394515223494, "id": 1 }, { "x": 339.13764510779436, "y": 566.1844094148132, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 339.13764510779436, "y": 566.1844094148132, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 578.7728026533997, "y": 469.9978406391708, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 339.13764510779436, "y": 566.1844094148132, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 638.4742951907131, "y": 26.378751889440725, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 332.5041459369818, "y": 73.64284171885122, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 339.13764510779436, "y": 566.1844094148132, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 638.4742951907131, "y": 26.378751889440725, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 339.13764510779436, "y": 566.1844094148132, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 80.25, "y": 215.4351112070827, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 37.3134328358209, "y": 32.1831137983157, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 80.25, "y": 215.4351112070827, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 80.25, "y": 215.4351112070827, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 927.860696517413, "y": 248.60289354351113, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 80.25, "y": 215.4351112070827, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 46.434494195688224, "y": 480.77736989851, "id": 3 }, { "x": 80.25, "y": 215.4351112070827, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 80.25, "y": 215.4351112070827, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 72.13930348258707, "y": 298.35456704815374, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 84.57711442786069, "y": 388.7367739149212, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 987.5621890547263, "y": 299.1837616065645, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 305.14096185737975, "y": 334.839127618225, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 736.318407960199, "y": 253.5780608939754, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 305.14096185737975, "y": 334.839127618225, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 736.318407960199, "y": 253.5780608939754, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 572.139303482587, "y": 334.839127618225, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 319.23714759535653, "y": 261.8700064780825, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 572.139303482587, "y": 334.839127618225, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 319.23714759535653, "y": 261.8700064780825, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 822.5538971807629, "y": 327.37637659252863, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 147.59535655058042, "y": 258.55322824443965, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 822.5538971807629, "y": 327.37637659252863, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 147.59535655058042, "y": 258.55322824443965, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 943.6152570480929, "y": 322.40120924206434, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 2.487562189054726, "y": 243.62772619304687, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 943.6152570480929, "y": 322.40120924206434, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 2.487562189054726, "y": 243.62772619304687, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 0, "y": 232.0190023752969, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 80.25, "y": 248.60289354351113, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 203.150912106136, "y": 148.27035197581515, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 104.4776119402985, "y": 256.0656445692075, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 204.80928689883913, "y": 255.2364500107968, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 449.4195688225539, "y": 290.89181602245736, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 706.4676616915423, "y": 169.00021593608292, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 25.70480928689884, "y": 15.59922263010149, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 635.1575456053068, "y": 551.2589073634205, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 919.75, "y": 425.22133448499244, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 857.379767827529, "y": 527.2122651695098, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 545.6053067993366, "y": 40.475059382422806, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 246.26865671641792, "y": 280.94148132152884, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 65.50580431177445, "y": 588.5726624919024, "id": 1 }, { "x": 160.03316749585406, "y": 256.89483912761824, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 246.26865671641792, "y": 280.94148132152884, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 529.8507462686567, "y": 170.65860505290433, "id": 1 }, { "x": 160.03316749585406, "y": 256.89483912761824, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 868.1592039800995, "y": 9.794860721226517, "id": 5 }, { "x": 246.26865671641792, "y": 280.94148132152884, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 529.8507462686567, "y": 170.65860505290433, "id": 1 }, { "x": 160.03316749585406, "y": 256.89483912761824, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 90.38142620232172, "y": 93.54351112070827, "id": 5 }, { "x": 246.26865671641792, "y": 280.94148132152884, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 529.8507462686567, "y": 170.65860505290433, "id": 1 }, { "x": 160.03316749585406, "y": 256.89483912761824, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 246.26865671641792, "y": 280.94148132152884, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 302.65339966832505, "y": 115.10256963938674, "id": 1 }, { "x": 160.03316749585406, "y": 256.89483912761824, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 246.26865671641792, "y": 280.94148132152884, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 302.65339966832505, "y": 115.10256963938674, "id": 1 }, { "x": 160.03316749585406, "y": 256.89483912761824, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 493.3665008291874, "y": 412.7834161088318, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 302.65339966832505, "y": 115.10256963938674, "id": 1 }, { "x": 432.8358208955224, "y": 61.20492334269056, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 493.3665008291874, "y": 412.7834161088318, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 646.7661691542288, "y": 410.2958324335997, "id": 1 }, { "x": 432.8358208955224, "y": 61.20492334269056, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 493.3665008291874, "y": 412.7834161088318, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 646.7661691542288, "y": 410.2958324335997, "id": 1 }, { "x": 837.4792703150912, "y": 497.36126106672424, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 493.3665008291874, "y": 412.7834161088318, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 646.7661691542288, "y": 410.2958324335997, "id": 1 }, { "x": 837.4792703150912, "y": 497.36126106672424, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 339.96683250414594, "y": 402.0038868494926, "id": 5 }, { "x": 503.3167495854063, "y": 586.0850788166703, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 646.7661691542288, "y": 410.2958324335997, "id": 1 }, { "x": 837.4792703150912, "y": 497.36126106672424, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 731.3432835820895, "y": 573.6471604405097, "id": 5 }, { "x": 503.3167495854063, "y": 586.0850788166703, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 305.97014925373134, "y": 71.9844526020298, "id": 1 }, { "x": 837.4792703150912, "y": 497.36126106672424, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 731.3432835820895, "y": 573.6471604405097, "id": 5 }, { "x": 503.3167495854063, "y": 586.0850788166703, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 305.97014925373134, "y": 71.9844526020298, "id": 1 }, { "x": 837.4792703150912, "y": 497.36126106672424, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 731.3432835820895, "y": 573.6471604405097, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 305.97014925373134, "y": 71.9844526020298, "id": 1 }, { "x": 837.4792703150912, "y": 497.36126106672424, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 150.91210613598673, "y": 102.66465126322609, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 305.97014925373134, "y": 71.9844526020298, "id": 1 }, { "x": 75.45605306799337, "y": 215.4351112070827, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 150.91210613598673, "y": 102.66465126322609, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 305.97014925373134, "y": 71.9844526020298, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 150.91210613598673, "y": 102.66465126322609, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 305.97014925373134, "y": 71.9844526020298, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 165.00829187396351, "y": 52.91297775858346, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 165.00829187396351, "y": 52.91297775858346, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 343.2835820895522, "y": 287.57503778881454, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 343.2835820895522, "y": 287.57503778881454, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 352.40464344941955, "y": 296.69617793133233, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 553.8971807628524, "y": 171.48779961131504, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 535.6550580431177, "y": 526.3830706110991, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 553.8971807628524, "y": 171.48779961131504, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 535.6550580431177, "y": 526.3830706110991, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 723.8805970149253, "y": 289.23342690563595, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 902.9850746268656, "y": 293.3793996976895, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 537.3134328358209, "y": 295.8669833729216, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 340.7960199004975, "y": 167.3418268192615, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 174.12935323383084, "y": 251.91967177715395, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 71.3101160862355, "y": 251.91967177715395, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 161.6915422885572, "y": 131.68646080760095, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 315.92039800995025, "y": 328.2055711509393, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 161.6915422885572, "y": 131.68646080760095, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 374.79270315091213, "y": 21.403584538976464, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 325.0414593698176, "y": 212.11833297343986, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 374.79270315091213, "y": 21.403584538976464, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 325.0414593698176, "y": 212.11833297343986, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 199.00497512437812, "y": 128.3696825739581, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 325.0414593698176, "y": 212.11833297343986, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 129.35323383084577, "y": 214.605916648672, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 325.0414593698176, "y": 212.11833297343986, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 319.23714759535653, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 350.74626865671644, "y": 212.11833297343986, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 319.23714759535653, "y": 212.11833297343986, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 304.31177446102816, "y": 210.45994385661845, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 349.08789386401327, "y": 210.45994385661845, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 349.08789386401327, "y": 210.45994385661845, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 349.08789386401327, "y": 210.45994385661845, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 413.76451077943614, "y": 212.94752753185057, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 382.2553897180763, "y": 183.096523429065, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 382.2553897180763, "y": 183.096523429065, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 382.2553897180763, "y": 183.096523429065, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 355.7213930348259, "y": 230.3606132584755, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 355.7213930348259, "y": 230.3606132584755, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 355.7213930348259, "y": 230.3606132584755, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 355.7213930348259, "y": 230.3606132584755, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 355.7213930348259, "y": 230.3606132584755, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 217.24709784411277, "y": 546.2837400129562, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 599.502487562189, "y": 279.28309220470743, "id": 1 }, { "x": 381.4262023217247, "y": 226.21464046642194, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 381.4262023217247, "y": 226.21464046642194, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 707.2968490878939, "y": 169.82941049449363, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 839.9668325041459, "y": 450.0971712373138, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 642.620232172471, "y": 113.44418052256532, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 881.4262023217248, "y": 146.61196285899373, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 642.620232172471, "y": 113.44418052256532, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 822.5538971807629, "y": 148.27035197581515, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 664.179104477612, "y": 141.63679550852947, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 743.7810945273632, "y": 334.839127618225, "id": 6 }, { "x": 822.5538971807629, "y": 148.27035197581515, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 664.179104477612, "y": 141.63679550852947, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 708.955223880597, "y": 290.89181602245736, "id": 6 }, { "x": 822.5538971807629, "y": 148.27035197581515, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 664.179104477612, "y": 141.63679550852947, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 708.955223880597, "y": 290.89181602245736, "id": 6 }, { "x": 926.2023217247098, "y": 144.95357374217232, "id": 7 }],
   	*/
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 752.9021558872305, "y": 105.98142949686893, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 708.955223880597, "y": 290.89181602245736, "id": 6 }, { "x": 926.2023217247098, "y": 144.95357374217232, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 708.955223880597, "y": 290.89181602245736, "id": 6 }, { "x": 926.2023217247098, "y": 144.95357374217232, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 708.955223880597, "y": 290.89181602245736, "id": 6 }, { "x": 893.8640132669983, "y": 135.0032390412438, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 708.955223880597, "y": 290.89181602245736, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 938.6401326699835, "y": 242.79853163463616, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 766.9983416252073, "y": 216.26430576549342, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 727.1973466003317, "y": 381.2740228892248, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 766.9983416252073, "y": 216.26430576549342, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 727.1973466003317, "y": 381.2740228892248, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 188.22553897180762, "y": 115.10256963938674, "id": 5 }, { "x": 758.7064676616916, "y": 252.74886633556468, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 727.1973466003317, "y": 381.2740228892248, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 326.69983416252074, "y": 236.16497516735046, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 758.7064676616916, "y": 252.74886633556468, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 727.1973466003317, "y": 381.2740228892248, "id": 0 }, { "x": 524.8756218905472, "y": 277.62470308788596, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 758.7064676616916, "y": 252.74886633556468, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 727.1973466003317, "y": 381.2740228892248, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 264.5107794361526, "y": 406.14985964154613, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 758.7064676616916, "y": 252.74886633556468, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],

    [{ "x": 727.1973466003317, "y": 381.2740228892248, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 758.7064676616916, "y": 252.74886633556468, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 689.8839137645108, "y": 397.0287194990283, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 758.7064676616916, "y": 252.74886633556468, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 689.8839137645108, "y": 397.0287194990283, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 757.0480928689884, "y": 231.1898078168862, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 757.0480928689884, "y": 231.1898078168862, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 252.90215588723052, "y": 217.92269488231483, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 780.2653399668325, "y": 231.1898078168862, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 227.19734660033168, "y": 231.1898078168862, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 780.2653399668325, "y": 231.1898078168862, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 227.19734660033168, "y": 231.1898078168862, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 227.19734660033168, "y": 231.1898078168862, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 227.19734660033168, "y": 231.1898078168862, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 132.66998341625208, "y": 112.61498596415461, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 135.98673300165837, "y": 52.91297775858346, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 113.59867330016584, "y": 59.546534225869145, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 113.59867330016584, "y": 59.546534225869145, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 113.59867330016584, "y": 59.546534225869145, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 113.59867330016584, "y": 59.546534225869145, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 737.9767827529022, "y": 237.82336428417187, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 737.1475953565506, "y": 254.4072554523861, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 737.1475953565506, "y": 254.4072554523861, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 737.1475953565506, "y": 254.4072554523861, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 213.93034825870646, "y": 384.5908011228676, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 739.6351575456054, "y": 370.49449362988554, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 257.0480928689884, "y": 416.92938890088533, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 257.0480928689884, "y": 416.92938890088533, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 433.66500829187396, "y": 236.99416972576117, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 432.0066334991708, "y": 245.28611530986828, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 432.0066334991708, "y": 245.28611530986828, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 432.0066334991708, "y": 245.28611530986828, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 440.2985074626866, "y": 237.82336428417187, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 159.20398009950247, "y": 116.76095875620817, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 440.2985074626866, "y": 237.82336428417187, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 121.06135986733001, "y": 130.02807169077954, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 440.2985074626866, "y": 237.82336428417187, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 121.06135986733001, "y": 130.02807169077954, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 440.2985074626866, "y": 237.82336428417187, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 440.2985074626866, "y": 237.82336428417187, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 734.6600331674958, "y": 78.61800906931548, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 903.8142620232172, "y": 117.59015331461887, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 272.8026533996683, "y": 243.62772619304687, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 257.0480928689884, "y": 227.04383502483265, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 236.318407960199, "y": 238.6525588425826, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 236.99416972576117, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 225.38544590801123, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 225.38544590801123, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 225.38544590801123, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 225.38544590801123, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 233.83084577114428, "y": 230.3606132584755, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 232.0190023752969, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 232.0190023752969, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 232.0190023752969, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 262.8524046434494, "y": 232.0190023752969, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 269.48590381426203, "y": 236.99416972576117, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 230.51409618573797, "y": 235.33578060893976, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 230.51409618573797, "y": 235.33578060893976, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 230.51409618573797, "y": 235.33578060893976, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 230.51409618573797, "y": 235.33578060893976, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 230.51409618573797, "y": 235.33578060893976, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 230.51409618573797, "y": 235.33578060893976, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 295.19071310116084, "y": 172.31699416972577, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 295.19071310116084, "y": 172.31699416972577, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 295.19071310116084, "y": 172.31699416972577, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 287.7280265339967, "y": 207.1431656229756, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 276.1194029850746, "y": 202.16799827251134, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 406.301824212272, "y": 270.1619520621896, "id": 1 }, { "x": 276.1194029850746, "y": 202.16799827251134, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 406.301824212272, "y": 270.1619520621896, "id": 1 }, { "x": 276.1194029850746, "y": 202.16799827251134, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 695.6882255389718, "y": 232.8481969337076, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 276.1194029850746, "y": 202.16799827251134, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 695.6882255389718, "y": 232.8481969337076, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 695.6882255389718, "y": 232.8481969337076, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 695.6882255389718, "y": 232.8481969337076, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 695.6882255389718, "y": 232.8481969337076, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 701.4925373134329, "y": 334.839127618225, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 701.4925373134329, "y": 334.839127618225, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 878.93864013267, "y": 61.20492334269056, "id": 7 }],
    [{ "x": 701.4925373134329, "y": 334.839127618225, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 719.7346600331675, "y": 52.91297775858346, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 878.93864013267, "y": 61.20492334269056, "id": 7 }],
    [{ "x": 701.4925373134329, "y": 334.839127618225, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 719.7346600331675, "y": 52.91297775858346, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 909.6185737976783, "y": 219.58108399913627, "id": 7 }],
    [{ "x": 701.4925373134329, "y": 334.839127618225, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 803.4825870646766, "y": 438.48844741956384, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 909.6185737976783, "y": 219.58108399913627, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 803.4825870646766, "y": 438.48844741956384, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 772.8026533996683, "y": 202.99719283092205, "id": 6 }, { "x": 909.6185737976783, "y": 219.58108399913627, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 803.4825870646766, "y": 438.48844741956384, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 909.6185737976783, "y": 219.58108399913627, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 784.4112769485904, "y": 174.8045778449579, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 909.6185737976783, "y": 219.58108399913627, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 784.4112769485904, "y": 174.8045778449579, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 692.3714759535656, "y": 308.30490174908226, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 784.4112769485904, "y": 174.8045778449579, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 692.3714759535656, "y": 308.30490174908226, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 692.3714759535656, "y": 308.30490174908226, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 692.3714759535656, "y": 308.30490174908226, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 692.3714759535656, "y": 308.30490174908226, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 785.240464344942, "y": 95.20190023752969, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 785.240464344942, "y": 95.20190023752969, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 886.4013266998342, "y": 177.29216152019004, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 922.0563847429519, "y": 72.81364716044051, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 922.0563847429519, "y": 72.81364716044051, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 613.5986733001658, "y": 70.32606348520838, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 877.2802653399668, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 384.742951907131, "y": 64.5217015763334, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 838.3084577114428, "y": 241.14014251781472, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 596.1857379767828, "y": 194.70524724681493, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 149.2537313432836, "y": 411.12502699201036, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 884.742951907131, "y": 333.1807385014036, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 208.12603648424545, "y": 247.77369898510042, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 907.9601990049752, "y": 487.4109263657957, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 375.62189054726366, "y": 266.84517382854676, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 61.359867330016584, "y": 555.404880155474, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 61.359867330016584, "y": 555.404880155474, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 169.98341625207297, "y": 89.39753832865472, "id": 5 }, { "x": 61.359867330016584, "y": 555.404880155474, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 72.96849087893864, "y": 553.7464910386526, "id": 5 }, { "x": 61.359867330016584, "y": 555.404880155474, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 72.96849087893864, "y": 553.7464910386526, "id": 5 }, { "x": 61.359867330016584, "y": 555.404880155474, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 72.96849087893864, "y": 553.7464910386526, "id": 5 }, { "x": 324.212271973466, "y": 295.0377888145109, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 324.212271973466, "y": 295.0377888145109, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 70.48092868988391, "y": 560.3800475059383, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 324.212271973466, "y": 295.0377888145109, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 394.6932006633499, "y": 212.11833297343986, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 324.212271973466, "y": 295.0377888145109, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 394.6932006633499, "y": 212.11833297343986, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 97.01492537313433, "y": 556.2340747138846, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 900.497512437811, "y": 37.15828114877996, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 784.4112769485904, "y": 364.6901317210106, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 927.860696517413, "y": 42.133448499244224, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 933.665008291874, "y": 23.061973655797885, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 784.4112769485904, "y": 364.6901317210106, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 612.7694859038143, "y": 256.89483912761824, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 784.4112769485904, "y": 364.6901317210106, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 946.9320066334992, "y": 24.720362772619303, "id": 1 }, { "x": 612.7694859038143, "y": 256.89483912761824, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 667.4958540630182, "y": 310.7924854243144, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 777.7777777777778, "y": 295.0377888145109, "id": 1 }, { "x": 612.7694859038143, "y": 256.89483912761824, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 667.4958540630182, "y": 310.7924854243144, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 777.7777777777778, "y": 295.0377888145109, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 667.4958540630182, "y": 310.7924854243144, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 777.7777777777778, "y": 295.0377888145109, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 532.3383084577115, "y": 172.31699416972577, "id": 0 }, { "x": 743.7810945273632, "y": 57.88814510904772, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 743.7810945273632, "y": 57.88814510904771, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 339.13764510779436, "y": 311.62167998272514, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 583.7479270315091, "y": 252.74886633556468, "id": 5 }, { "x": 365.67164179104475, "y": 256.89483912761824, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 583.7479270315091, "y": 252.74886633556468, "id": 5 }, { "x": 605.3067993366501, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 583.7479270315091, "y": 252.74886633556468, "id": 5 }, { "x": 605.3067993366501, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 583.7479270315091, "y": 252.74886633556468, "id": 5 }, { "x": 607.7943615257049, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 583.7479270315091, "y": 252.74886633556468, "id": 5 }, { "x": 607.7943615257049, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 584.5771144278607, "y": 291.72101058086804, "id": 5 }, { "x": 607.7943615257049, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 777.7777777777778, "y": 94.37270567911898, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 584.5771144278607, "y": 291.72101058086804, "id": 5 }, { "x": 607.7943615257049, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 584.5771144278607, "y": 291.72101058086804, "id": 5 }, { "x": 607.7943615257049, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 607.7943615257049, "y": 225.38544590801123, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 644.2786069651742, "y": 174.8045778449579, "id": 7 }],
    [{ "x": 758.7064676616916, "y": 178.95055063701145, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 493.3665008291874, "y": 66.18009069315482, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 605.3067993366501, "y": 17.257611746922912, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 430.3482587064677, "y": 169.82941049449363, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 834.9917081260364, "y": 196.36363636363637, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 761.1940298507462, "y": 61.20492334269056, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 560.530679933665, "y": 73.64284171885122, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 432.0066334991708, "y": 198.02202548045778, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 730.514096185738, "y": 291.72101058086804, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 653.3996683250415, "y": 71.9844526020298, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 585.4063018242123, "y": 371.3236881882963, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 650.0829187396351, "y": 370.49449362988554, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 650.0829187396351, "y": 370.49449362988554, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 650.0829187396351, "y": 370.49449362988554, "id": 4 }, { "x": 485.9038142620232, "y": 277.62470308788596, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 650.0829187396351, "y": 370.49449362988554, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 659.2039800995025, "y": 211.28913841502916, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 709.7844112769486, "y": 125.05290434031527, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 622.7197346600332, "y": 226.21464046642194, "id": 7 }],
    [{ "x": 760.3648424543946, "y": 198.8512200388685, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 622.7197346600332, "y": 226.21464046642194, "id": 7 }],
    [{ "x": 383.0845771144279, "y": 378.78643921399265, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 837.4792703150912, "y": 201.33880371410064, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 622.7197346600332, "y": 226.21464046642194, "id": 7 }],
    [{ "x": 383.0845771144279, "y": 378.78643921399265, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 702.3217247097845, "y": 73.64284171885122, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 622.7197346600332, "y": 226.21464046642194, "id": 7 }],
    [{ "x": 733.8308457711443, "y": 207.9723601813863, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 702.3217247097845, "y": 73.64284171885122, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 622.7197346600332, "y": 226.21464046642194, "id": 7 }],
    [{ "x": 733.8308457711443, "y": 207.9723601813863, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 622.7197346600332, "y": 226.21464046642194, "id": 7 }],
    [{ "x": 733.8308457711443, "y": 207.9723601813863, "id": 0 }, { "x": 569.6517412935324, "y": 343.1310732023321, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 733.8308457711443, "y": 207.9723601813863, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 495.85406301824213, "y": 268.5035629453682, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 733.8308457711443, "y": 207.9723601813863, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 519.0713101160862, "y": 239.4817534009933, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 733.8308457711443, "y": 207.9723601813863, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 602.8192371475953, "y": 286.7458432304038, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 657.5456053067993, "y": 152.4163247678687, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 490.8789386401327, "y": 238.6525588425826, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 588.7230514096186, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 490.8789386401327, "y": 238.6525588425826, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 588.7230514096186, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 490.8789386401327, "y": 238.6525588425826, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 490.8789386401327, "y": 238.6525588425826, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 490.8789386401327, "y": 238.6525588425826, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 490.8789386401327, "y": 238.6525588425826, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 558.0431177446103, "y": 232.8481969337076, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 558.0431177446103, "y": 232.8481969337076, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 728.0265339966833, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 640.1326699834162, "y": 232.8481969337076, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 728.0265339966833, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 813.4328358208955, "y": 246.11530986827898, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 728.0265339966833, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 813.4328358208955, "y": 246.11530986827898, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 728.0265339966833, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 891.3764510779437, "y": 255.2364500107968, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 728.0265339966833, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 891.3764510779437, "y": 255.2364500107968, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 728.0265339966833, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 891.3764510779437, "y": 255.2364500107968, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 813.4328358208955, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 717.2470978441128, "y": 131.68646080760095, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 813.4328358208955, "y": 240.31094795940402, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 717.2470978441128, "y": 131.68646080760095, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 717.2470978441128, "y": 131.68646080760095, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 182.4212271973466, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 203.98009950248758, "y": 450.0971712373138, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 174.95854063018243, "y": 451.7555603541352, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 174.95854063018243, "y": 451.7555603541352, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 851.575456053068, "y": 416.92938890088533, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 862.3548922056385, "y": 426.0505290434032, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 549.7512437810946, "y": 266.84517382854676, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 486.7330016583748, "y": 232.0190023752969, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 198.17578772802653, "y": 448.43878212049236, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 474.29519071310114, "y": 447.60958756208163, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 892.2056384742951, "y": 416.10019434247465, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 436.0008637443317, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 745.4394693200663, "y": 437.6592528611531, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 354.0630182421227, "y": 448.43878212049236, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 352.40464344941955, "y": 381.2740228892248, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 475.1243781094527, "y": 441.80522565320666, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 893.8640132669983, "y": 418.58777801770674, "id": 2 }, { "x": 352.40464344941955, "y": 381.2740228892248, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 354.8922056384743, "y": 371.3236881882963, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 187.39635157545607, "y": 385.41999568127835, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 645.1077943615257, "y": 323.2304038004751, "id": 2 }, { "x": 352.40464344941955, "y": 381.2740228892248, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 354.8922056384743, "y": 371.3236881882963, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 645.1077943615257, "y": 323.2304038004751, "id": 2 }, { "x": 352.40464344941955, "y": 381.2740228892248, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 354.8922056384743, "y": 371.3236881882963, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 645.1077943615257, "y": 323.2304038004751, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 354.8922056384743, "y": 371.3236881882963, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 645.1077943615257, "y": 323.2304038004751, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 490.0497512437811, "y": 373.8112718635284, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 613.5986733001658, "y": 368.83610451306413, "id": 1 }, { "x": 645.1077943615257, "y": 323.2304038004751, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 645.1077943615257, "y": 323.2304038004751, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 729.6849087893864, "y": 369.66529907147486, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 839.1376451077944, "y": 365.5193262794213, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 488.3913764510779, "y": 293.3793996976895, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 549.7512437810946, "y": 289.23342690563595, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 669.983416252073, "y": 292.5502051392788, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 368.9883913764511, "y": 289.23342690563595, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 793.5323383084577, "y": 292.5502051392788, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 349.91708126036485, "y": 285.0874541135824, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 192.3714759535655, "y": 286.7458432304038, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 166.66666666666666, "y": 175.6337724033686, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 866.5008291873963, "y": 260.21161736126106, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 166.66666666666666, "y": 175.6337724033686, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 138.4742951907131, "y": 177.29216152019004, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 138.4742951907131, "y": 177.29216152019004, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 138.4742951907131, "y": 177.29216152019004, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 138.4742951907131, "y": 177.29216152019004, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 138.4742951907131, "y": 177.29216152019004, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 138.4742951907131, "y": 177.29216152019004, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 131.8407960199005, "y": 81.10559274454762, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 131.8407960199005, "y": 81.10559274454762, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 317.5787728026534, "y": 161.53746491038652, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 504.97512437810946, "y": 185.58410710429712, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 493.3665008291874, "y": 83.59317641977975, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 423.71475953565505, "y": 164.02504858561866, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 622.7197346600332, "y": 177.29216152019004, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 707.2968490878939, "y": 164.02504858561866, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 795.1907131011609, "y": 183.9257179874757, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 764.5107794361526, "y": 83.59317641977975, "id": 3 }, { "x": 906.301824212272, "y": 174.8045778449579, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 764.5107794361526, "y": 83.59317641977975, "id": 3 }, { "x": 873.9635157545605, "y": 72.81364716044051, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 764.5107794361526, "y": 83.59317641977975, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 735.4892205638474, "y": 245.28611530986828, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 735.4892205638474, "y": 245.28611530986828, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 735.4892205638474, "y": 245.28611530986828, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 686.5671641791045, "y": 71.9844526020298, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 603.6484245439469, "y": 63.69250701792269, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 735.4892205638474, "y": 245.28611530986828, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 622.7197346600332, "y": 232.0190023752969, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 735.4892205638474, "y": 245.28611530986828, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 622.7197346600332, "y": 232.0190023752969, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 735.4892205638474, "y": 245.28611530986828, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 555.5555555555555, "y": 292.5502051392788, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 699.8341625207297, "y": 303.32973439861803, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 555.5555555555555, "y": 292.5502051392788, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 708.955223880597, "y": 262.6992010364932, "id": 3 }, { "x": 898.8391376451078, "y": 272.64953573742173, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 555.5555555555555, "y": 292.5502051392788, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 708.955223880597, "y": 262.6992010364932, "id": 3 }, { "x": 882.2553897180762, "y": 243.62772619304687, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 555.5555555555555, "y": 292.5502051392788, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 708.955223880597, "y": 262.6992010364932, "id": 3 }, { "x": 882.2553897180762, "y": 243.62772619304687, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 585.4063018242123, "y": 241.96933707622543, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 647.5953565505804, "y": 425.22133448499244, "id": 3 }, { "x": 882.2553897180762, "y": 243.62772619304687, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 585.4063018242123, "y": 241.96933707622543, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 647.5953565505804, "y": 425.22133448499244, "id": 3 }, { "x": 833.3333333333334, "y": 428.53811271863526, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 585.4063018242123, "y": 241.96933707622543, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 647.5953565505804, "y": 425.22133448499244, "id": 3 }, { "x": 833.3333333333334, "y": 428.53811271863526, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 495.02487562189054, "y": 421.0753616929389, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 833.3333333333334, "y": 428.53811271863526, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 495.02487562189054, "y": 421.0753616929389, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 495.02487562189054, "y": 421.0753616929389, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 641.7910447761194, "y": 66.18009069315482, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 490.0497512437811, "y": 80.2763981861369, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 555.5555555555555, "y": 92.71431656229755, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 376.45107794361525, "y": 65.35089613474412, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 291.04477611940297, "y": 83.59317641977975, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 626.0364842454395, "y": 215.4351112070827, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 127.69485903814262, "y": 84.42237097819046, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 626.0364842454395, "y": 215.4351112070827, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 217.92269488231483, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 626.0364842454395, "y": 215.4351112070827, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 217.92269488231483, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 569.6517412935324, "y": 406.9790541999568, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 217.92269488231483, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 641.7910447761194, "y": 553.7464910386526, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 217.92269488231483, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 806.7993366500829, "y": 556.2340747138846, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 217.92269488231483, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 554.726368159204, "y": 125.05290434031527, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 650.0829187396351, "y": 474.1438134312244, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 217.92269488231483, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 650.0829187396351, "y": 474.1438134312244, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 846.6003316749585, "y": 549.600518246599, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 650.0829187396351, "y": 474.1438134312244, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 446.93200663349916, "y": 125.05290434031527, "id": 7 }],
    [{ "x": 846.6003316749585, "y": 549.600518246599, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 650.0829187396351, "y": 474.1438134312244, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 650.0829187396351, "y": 474.1438134312244, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 632.6699834162521, "y": 238.6525588425826, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 576.285240464345, "y": 190.55927445476138, "id": 5 }, { "x": 852.4046434494196, "y": 359.71496437054634, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 458.54063018242124, "y": 344.78946231915353, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 576.285240464345, "y": 190.55927445476138, "id": 5 }, { "x": 258.70646766169153, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 577.1144278606965, "y": 346.44785143597494, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 576.285240464345, "y": 190.55927445476138, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 576.285240464345, "y": 190.55927445476138, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 357.379767827529, "y": 227.87302958324335, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 480.0995024875622, "y": 352.2522133448499, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 754.5605306799337, "y": 222.06866767436838, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 480.0995024875622, "y": 352.2522133448499, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 480.0995024875622, "y": 352.2522133448499, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 579.6019900497513, "y": 267.67436838695744, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 849.9170812603649, "y": 124.22370978190456, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 708.955223880597, "y": 103.4938458216368, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 258.7064676616915, "y": 415.2709997840639, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 384.742951907131, "y": 498.190455625135, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 692.3714759535656, "y": 140.80760095011877, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 598.6733001658375, "y": 493.2152882746707, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 692.3714759535656, "y": 140.80760095011877, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 736.318407960199, "y": 349.7646296696178, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 692.3714759535656, "y": 140.80760095011877, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 877.2802653399668, "y": 361.37335348736775, "id": 5 }, { "x": 692.3714759535656, "y": 140.80760095011877, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 692.3714759535656, "y": 140.80760095011877, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 847.4295190713101, "y": 72.81364716044051, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 643.4494195688226, "y": 420.2461671345282, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 852.4046434494196, "y": 225.38544590801123, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 643.4494195688226, "y": 420.2461671345282, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 811.7744610281924, "y": 484.9233426905636, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 469.32006633499174, "y": 431.0256963938674, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 643.4494195688226, "y": 420.2461671345282, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 469.32006633499174, "y": 431.0256963938674, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 643.4494195688226, "y": 420.2461671345282, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 393.86401326699837, "y": 507.31159576765276, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 286.8988391376451, "y": 63.69250701792269, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 257.87728026533995, "y": 269.3327575037789, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 776.1194029850747, "y": 273.47873029583246, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 899.6683250414594, "y": 266.01597927013603, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 402.9850746268657, "y": 437.6592528611531, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 885.5721393034826, "y": 171.48779961131504, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 402.9850746268657, "y": 437.6592528611531, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 86.23548922056385, "y": 269.3327575037789, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 885.5721393034826, "y": 171.48779961131504, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 402.9850746268657, "y": 437.6592528611531, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 565.5058043117745, "y": 436.8300583027424, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 885.5721393034826, "y": 171.48779961131504, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 320.0663349917081, "y": 473.31461887281364, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 459.3698175787728, "y": 522.2370978190455, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 885.5721393034826, "y": 171.48779961131504, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 320.0663349917081, "y": 473.31461887281364, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 459.3698175787728, "y": 522.2370978190455, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 885.5721393034826, "y": 171.48779961131504, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 290.2155887230514, "y": 504.8240120924207, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 416.2520729684909, "y": 546.2837400129562, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 885.5721393034826, "y": 171.48779961131504, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 290.2155887230514, "y": 504.8240120924207, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 603.6484245439469, "y": 285.9166486719931, "id": 0 }, { "x": 416.2520729684909, "y": 546.2837400129562, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 290.2155887230514, "y": 504.8240120924207, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 416.2520729684909, "y": 546.2837400129562, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 290.2155887230514, "y": 504.8240120924207, "id": 5 }, { "x": 603.6484245439469, "y": 283.429064996761, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 416.2520729684909, "y": 546.2837400129562, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 290.2155887230514, "y": 504.8240120924207, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 677.4461028192371, "y": 515.6035413517599, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 290.2155887230514, "y": 504.8240120924207, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 677.4461028192371, "y": 515.6035413517599, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 329.18739635157544, "y": 523.895486935867, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 503.3167495854063, "y": 551.2589073634205, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 329.18739635157544, "y": 523.895486935867, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 503.3167495854063, "y": 551.2589073634205, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 233.83084577114428, "y": 568.6719930900454, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 322.5538971807629, "y": 517.2619304685813, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 233.83084577114428, "y": 568.6719930900454, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 322.5538971807629, "y": 517.2619304685813, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 468.49087893864015, "y": 549.600518246599, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 310.1160862354892, "y": 559.5508529475276, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 468.49087893864015, "y": 549.600518246599, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 310.1160862354892, "y": 559.5508529475276, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 349.91708126036485, "y": 465.0226732887065, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 249.5854063018242, "y": 513.9451522349385, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 349.91708126036485, "y": 465.0226732887065, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 249.5854063018242, "y": 513.9451522349385, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 374.79270315091213, "y": 528.0414597279206, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 249.5854063018242, "y": 513.9451522349385, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 339.13764510779436, "y": 518.091125026992, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 179.93366500829188, "y": 118.41934787302958, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 339.13764510779436, "y": 518.091125026992, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 783.5820895522388, "y": 114.27337508097604, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 339.13764510779436, "y": 518.091125026992, "id": 1 }, { "x": 449.4195688225539, "y": 272.64953573742173, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 339.13764510779436, "y": 518.091125026992, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 339.13764510779436, "y": 518.091125026992, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 254.56053067993366, "y": 507.31159576765276, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 480.92868988391376, "y": 534.6750161952062, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 254.56053067993366, "y": 507.31159576765276, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 392.2056384742952, "y": 533.8458216367956, "id": 5 }, { "x": 555.5555555555555, "y": 312.4508745411358, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 254.56053067993366, "y": 507.31159576765276, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 392.2056384742952, "y": 533.8458216367956, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 254.56053067993366, "y": 507.31159576765276, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 354.8922056384743, "y": 586.0850788166703, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 228.85572139303483, "y": 592.7186352839559, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 354.8922056384743, "y": 586.0850788166703, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 228.85572139303483, "y": 592.7186352839559, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 318.40796019900495, "y": 494.8736773914921, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 318.40796019900495, "y": 494.8736773914921, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 318.40796019900495, "y": 494.8736773914921, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 316.74958540630183, "y": 482.4357590153315, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 377.28026533996683, "y": 455.072338587778, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 323.3830845771144, "y": 488.24012092420645, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 323.3830845771144, "y": 488.24012092420645, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 367.3300165837479, "y": 450.92636579572445, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 367.3300165837479, "y": 450.92636579572445, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 312.60364842454396, "y": 488.24012092420645, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 312.60364842454396, "y": 488.24012092420645, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 312.60364842454396, "y": 488.24012092420645, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 389.71807628524044, "y": 459.21831137983156, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 170.81260364842456, "y": 169.00021593608292, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 762.8524046434494, "y": 106.81062405527963, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 389.71807628524044, "y": 459.21831137983156, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 217.24709784411277, "y": 142.46599006694018, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 736.318407960199, "y": 80.2763981861369, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 389.71807628524044, "y": 459.21831137983156, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 217.24709784411277, "y": 142.46599006694018, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 736.318407960199, "y": 80.2763981861369, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 389.71807628524044, "y": 459.21831137983156, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 208.955223880597, "y": 156.56229755992226, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 736.318407960199, "y": 80.2763981861369, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 365.67164179104475, "y": 469.1686460807601, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 208.955223880597, "y": 156.56229755992226, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 736.318407960199, "y": 80.2763981861369, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 386.40132669983416, "y": 437.6592528611531, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 208.955223880597, "y": 156.56229755992226, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 736.318407960199, "y": 80.2763981861369, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 208.955223880597, "y": 156.56229755992226, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 736.318407960199, "y": 80.2763981861369, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 132.78688524590166, "y": 183.6065573770492, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 340.43715846994536, "y": 113.66120218579236, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 132.78688524590166, "y": 183.6065573770492, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 267.2131147540984, "y": 231.69398907103826, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 132.78688524590166, "y": 183.6065573770492, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 267.2131147540984, "y": 231.69398907103826, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 133.87978142076503, "y": 283.0601092896175, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 260.65573770491807, "y": 212.0218579234973, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 133.87978142076503, "y": 283.0601092896175, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 324.0437158469946, "y": 115.84699453551913, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 133.87978142076503, "y": 283.0601092896175, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 324.0437158469946, "y": 115.84699453551913, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 190.71038251366122, "y": 203.2786885245902, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 704.0996676464373, "y": 253.89344262295083, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 190.71038251366122, "y": 203.2786885245902, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 638.3205578929168, "y": 154.30327868852459, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 190.71038251366122, "y": 203.2786885245902, "id": 7 }],
    [{ "x": 819.2371475953565, "y": 356.39818613690346, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 126.50602409638556, "y": 113.25301204819279, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 801.2048192771085, "y": 373.4939759036145, "id": 0 }, { "x": 424.54394693200663, "y": 246.9445044266897, "id": 1 }, { "x": 259.5356550580431, "y": 208.801554739797, "id": 2 }, { "x": 716.4179104477612, "y": 93.54351112070827, "id": 3 }, { "x": 247.92703150912106, "y": 389.5659684733319, "id": 4 }, { "x": 163.34991708126037, "y": 106.81062405527963, "id": 5 }, { "x": 762.0232172470978, "y": 230.3606132584755, "id": 6 }, { "x": 901.3266998341625, "y": 151.587130209458, "id": 7 }],
    [{ "x": 540.6301824212272, "y": 312.4508745411358, "id": 0 }, { "x": 342.45439469320064, "y": 475.8022025480458, "id": 1 }, { "x": 417.910447761194, "y": 290.89181602245736, "id": 2 }, { "x": 671.0843373493976, "y": 151.80722891566268, "id": 3 }, { "x": 423.71475953565505, "y": 287.57503778881454, "id": 4 }, { "x": 368.1592039800995, "y": 462.53508961347444, "id": 5 }, { "x": 525.7048092868988, "y": 326.5471820341179, "id": 6 }, { "x": 190.71038251366122, "y": 203.2786885245902, "id": 7 }]
];

jam = function() {
	masker();
    var ccc = ssss.reverse();
    for (var i = shapes.length - 1; i >= 0; i--) {
        shapes[i].synth.set({
            "envelope" : {
                "attack" : 0,
                "decay" : 1,
                "sustain" : 1,
                "release" : 0,
            }            
        })
    }
    playing('all', 60, ccc, function() {

        for (var i = shapes.length - 1; i >= 0; i--) {
            shapes[i].synth.set({
                "envelope" : {
                    "attack" : 2,
                    "decay" : 20,
                    "sustain" : 20,
                    "release" : .2,
                }            
            })
        }

        playing(1,2000, false, function(){
        	removeGnar();
            for (var i = shapes.length - 1; i >= 0; i--) {
                shapes[i].synth.set(shapeSynthDefault);
            }            
            
        })
    })
};


var gnarSprites = [];

var gnarTex1 = PIXI.Texture.fromImage('/static/img/drag/losmen/computerarena.jpg');
var gnarTex2 = PIXI.Texture.fromImage('/static/img/drag/losmen/fieldfam.jpg');
var gnarTex3 = PIXI.Texture.fromImage('/static/img/drag/losmen/jamcar.jpg');
var gnarTex4 = PIXI.Texture.fromImage('/static/img/drag/losmen/signgirl.jpg');
var gnarTex5 = PIXI.Texture.fromImage('/static/img/drag/losmen/synthman.jpg');
var gnarTex6 = PIXI.Texture.fromImage('/static/img/drag/losmen/synthpool.jpg');
var gnarTex7 = PIXI.Texture.fromImage('/static/img/drag/losmen/tentfam.jpg');
var gnarTex8 = PIXI.Texture.fromImage('/static/img/drag/losmen/waveride.jpg');

var gnarImgs = [
    gnarTex1,
    gnarTex2,
    gnarTex3,
    gnarTex4,
    gnarTex5,
    gnarTex6,
    gnarTex7,
    gnarTex8
]
var gnarTexNo = 0;

masker = function(){

    for (var i = shapes.length - 1; i >= 0; i--) {

        if (gnarTexNo < notes.length -1) {
            gnarTexNo ++;
        } else {
            gnarTexNo = 0;
        }        
        var gnar = new PIXI.Sprite(gnarImgs[gnarTexNo]);
        gnar.mask = shapes[i];
        stage.addChild(gnar);  
        gnarSprites.push(gnar);  
    }  
}

function removeGnar() {
	for (var i = gnarSprites.length - 1; i >= 0; i--) {
		gnarSprites[i].mask = null;
		stage.removeChild(gnarSprites[i]);
	}
}

var jahSprites = [];
var jahColors = [
	0xFF0000,
	0xFFFF00,
	0x00FF00,
	0x00FFFF,
	0xFF700B
];

var jahColorNo = 0;


function jahCray() {


    for (var i = shapes.length - 1; i >= 0; i--) {

        if (jahColorNo < jahColors.length -1) {
            jahColorNo ++;
        } else {
            jahColorNo = 0;
        }     

		var jahGraph = new PIXI.Graphics();
		jahGraph.beginFill(jahColors[jahColorNo]);
		jahGraph.drawRect(0, 0, canvasWidth, canvasHeight);
		jahGraph.mask = shapes[i];
		stage.addChild(jahGraph);
        jahSprites.push(jahGraph);   
    }  



	TweenMax.to(cssBg, .3, {backgroundColor:'#000000', ease:Linear.easeNone});  
	playing(100, 0, false, false);
	setTimeout(function(){
		playing(100, 0, false, function(){
			for (var i = jahSprites.length - 1; i >= 0; i--) {
				jahSprites[i].mask = null;
				stage.removeChild(jahSprites[i]);
			}
			endPlaying();			
		});
	},1500);      
}

jCray = function(){
	jahCray();
}
var slowrideArray = [
    [{ "id": 0, "x": 195.45454545454547, "y": 485.1239669421488 }, { "id": 1, "x": 492.9752066115703, "y": 300 }, { "id": 2, "x": 726.0330578512397, "y": 501.6528925619835 }, { "id": 3, "x": 603.7190082644628, "y": 145.45454545454547 }, { "id": 4, "x": 407.02479338842977, "y": 503.305785123967 }, { "id": 5, "x": 191.3223140495868, "y": 184.29752066115702 }, { "id": 6, "x": 611.1570247933885, "y": 483.4710743801653 }, { "id": 7, "x": 316.9421487603306, "y": 211.5702479338843 }],
    [{ "id": 0, "x": 473.1404958677686, "y": 359.504132231405 }, { "id": 1, "x": 235.12396694214877, "y": 82.64462809917356 }, { "id": 2, "x": 693.801652892562, "y": 450.41322314049586 }, { "id": 3, "x": 682.2314049586777, "y": 256.198347107438 }, { "id": 4, "x": 437.603305785124, "y": 283.4710743801653 }, { "id": 5, "x": 712.8099173553719, "y": 129.7520661157025 }, { "id": 6, "x": 211.98347107438016, "y": 476.8595041322314 }, { "id": 7, "x": 232.64462809917356, "y": 223.1404958677686 }],
    [{ "id": 0, "x": 506.19834710743805, "y": 262.8099173553719 }, { "id": 1, "x": 746.6942148760331, "y": 119.83471074380165 }, { "id": 2, "x": 289.6694214876033, "y": 428.099173553719 }, { "id": 3, "x": 235.12396694214877, "y": 323.1404958677686 }, { "id": 4, "x": 754.1322314049587, "y": 349.58677685950414 }, { "id": 5, "x": 250, "y": 180.16528925619835 }, { "id": 6, "x": 730.9917355371902, "y": 480.9917355371901 }, { "id": 7, "x": 745.8677685950413, "y": 250.4132231404959 }],
    [{ "id": 0, "x": 511.9834710743802, "y": 143.80165289256198 }, { "id": 1, "x": 406.19834710743805, "y": 306.6115702479339 }, { "id": 2, "x": 514.4628099173553, "y": 138.84297520661158 }, { "id": 3, "x": 770.6611570247934, "y": 309.91735537190084 }, { "id": 4, "x": 524.3801652892562, "y": 471.90082644628103 }, { "id": 5, "x": 640.9090909090909, "y": 311.57024793388433 }, { "id": 6, "x": 516.9421487603306, "y": 479.3388429752066 }, { "id": 7, "x": 511.1570247933884, "y": 319.8347107438017 }],
    [{ "id": 0, "x": 526.8595041322315, "y": 454.54545454545456 }, { "id": 1, "x": 297.10743801652893, "y": 130.5785123966942 }, { "id": 2, "x": 738.4297520661157, "y": 156.19834710743802 }, { "id": 3, "x": 770.6611570247934, "y": 309.91735537190084 }, { "id": 4, "x": 230.16528925619835, "y": 294.21487603305786 }, { "id": 5, "x": 164.87603305785123, "y": 128.099173553719 }, { "id": 6, "x": 854.9586776859504, "y": 160.3305785123967 }, { "id": 7, "x": 522.7272727272727, "y": 342.97520661157023 }],
    [{ "id": 0, "x": 522.7272727272727, "y": 347.93388429752065 }, { "id": 1, "x": 807.0247933884298, "y": 306.6115702479339 }, { "id": 2, "x": 122.72727272727273, "y": 419.0082644628099 }, { "id": 3, "x": 861.5702479338843, "y": 190.0826446280992 }, { "id": 4, "x": 522.7272727272727, "y": 253.71900826446281 }, { "id": 5, "x": 811.1570247933885, "y": 447.93388429752065 }, { "id": 6, "x": 206.19834710743802, "y": 453.71900826446284 }, { "id": 7, "x": 533.4710743801653, "y": 457.02479338842977 }],
    [{ "id": 0, "x": 392.1487603305785, "y": 112.39669421487604 }, { "id": 1, "x": 645.0413223140496, "y": 204.95867768595042 }, { "id": 2, "x": 211.15702479338844, "y": 376.8595041322314 }, { "id": 3, "x": 711.9834710743802, "y": 334.7107438016529 }, { "id": 4, "x": 202.06611570247935, "y": 114.04958677685951 }, { "id": 5, "x": 803.7190082644628, "y": 212.39669421487605 }, { "id": 6, "x": 672.3140495867768, "y": 493.38842975206614 }, { "id": 7, "x": 292.97520661157023, "y": 228.92561983471074 }],
    [{ "id": 0, "x": 502.89256198347107, "y": 459.504132231405 }, { "id": 1, "x": 438.4297520661157, "y": 206.61157024793388 }, { "id": 2, "x": 206.19834710743802, "y": 394.21487603305786 }, { "id": 3, "x": 435.9504132231405, "y": 344.6280991735537 }, { "id": 4, "x": 550, "y": 534.7107438016529 }, { "id": 5, "x": 586.3636363636364, "y": 211.5702479338843 }, { "id": 6, "x": 749.1735537190083, "y": 430.57851239669424 }, { "id": 7, "x": 571.4876033057851, "y": 340.4958677685951 }],
    [{ "id": 0, "x": 169.00826446280993, "y": 559.5041322314049 }, { "id": 1, "x": 160.74380165289256, "y": 349.58677685950414 }, { "id": 2, "x": 852.4793388429753, "y": 505.78512396694214 }, { "id": 3, "x": 367.35537190082647, "y": 263.6363636363636 }, { "id": 4, "x": 834.297520661157, "y": 76.03305785123968 }, { "id": 5, "x": 852.4793388429753, "y": 322.3140495867769 }, { "id": 6, "x": 148.34710743801654, "y": 117.35537190082646 }, { "id": 7, "x": 597.9338842975206, "y": 214.87603305785126 }],
    [{ "id": 0, "x": 572.3140495867768, "y": 194.21487603305786 }, { "id": 1, "x": 436.77685950413223, "y": 80.16528925619835 }, { "id": 2, "x": 573.9669421487604, "y": 319.0082644628099 }, { "id": 3, "x": 542.5619834710744, "y": 483.4710743801653 }, { "id": 4, "x": 375.6198347107438, "y": 197.5206611570248 }, { "id": 5, "x": 623.5537190082645, "y": 89.25619834710744 }, { "id": 6, "x": 426.0330578512397, "y": 333.88429752066116 }, { "id": 7, "x": 411.1570247933884, "y": 482.6446280991736 }],
    [{ "id": 0, "x": 572.3140495867768, "y": 194.21487603305786 }, { "id": 1, "x": 509.504132231405, "y": 387.60330578512395 }, { "id": 2, "x": 814.4628099173553, "y": 383.4710743801653 }, { "id": 3, "x": 573.1404958677687, "y": 304.9586776859504 }, { "id": 4, "x": 375.6198347107438, "y": 197.5206611570248 }, { "id": 5, "x": 416.11570247933884, "y": 499.1735537190083 }, { "id": 6, "x": 154.13223140495867, "y": 404.9586776859504 }, { "id": 7, "x": 360.74380165289256, "y": 319.0082644628099 }],
    [{ "id": 0, "x": 575.6198347107438, "y": 209.0909090909091 }, { "id": 1, "x": 497.93388429752065, "y": 418.1818181818182 }, { "id": 2, "x": 813.6363636363636, "y": 305.78512396694214 }, { "id": 3, "x": 601.2396694214876, "y": 333.88429752066116 }, { "id": 4, "x": 375.6198347107438, "y": 180.9917355371901 }, { "id": 5, "x": 382.2314049586777, "y": 488.4297520661157 }, { "id": 6, "x": 135.9504132231405, "y": 467.7685950413223 }, { "id": 7, "x": 340.08264462809916, "y": 303.3057851239669 }],
    [{ "id": 0, "x": 589.6694214876034, "y": 138.01652892561984 }, { "id": 1, "x": 526.0330578512397, "y": 357.02479338842977 }, { "id": 2, "x": 815.2892561983472, "y": 406.6115702479339 }, { "id": 3, "x": 552.4793388429753, "y": 256.198347107438 }, { "id": 4, "x": 388.8429752066116, "y": 142.1487603305785 }, { "id": 5, "x": 444.21487603305786, "y": 455.3719008264463 }, { "id": 6, "x": 188.01652892561984, "y": 246.28099173553719 }, { "id": 7, "x": 413.6363636363636, "y": 252.89256198347107 }],
    [{ "id": 0, "x": 738.4297520661157, "y": 119.83471074380165 }, { "id": 1, "x": 462.39669421487605, "y": 402.4793388429752 }, { "id": 2, "x": 182.2314049586777, "y": 254.54545454545456 }, { "id": 3, "x": 626.8595041322315, "y": 350.41322314049586 }, { "id": 4, "x": 688.0165289256198, "y": 195.86776859504133 }, { "id": 5, "x": 360.74380165289256, "y": 505.78512396694214 }, { "id": 6, "x": 166.52892561983472, "y": 154.54545454545456 }, { "id": 7, "x": 504.54545454545456, "y": 258.6776859504132 }],
    [{ "id": 0, "x": 199.5867768595042, "y": 102.47933884297521 }, { "id": 1, "x": 530.9917355371902, "y": 242.97520661157026 }, { "id": 2, "x": 929.3388429752066, "y": 517.3553719008264 }, { "id": 3, "x": 779.7520661157025, "y": 412.39669421487605 }, { "id": 4, "x": 202.89256198347107, "y": 185.12396694214877 }, { "id": 5, "x": 387.1900826446281, "y": 180.16528925619835 }, { "id": 6, "x": 871.4876033057851, "y": 479.3388429752066 }, { "id": 7, "x": 666.5289256198347, "y": 345.45454545454544 }],
    [{ "id": 0, "x": 836.7768595041323, "y": 68.59504132231405 }, { "id": 1, "x": 903.7190082644628, "y": 512.396694214876 }, { "id": 2, "x": 698.7603305785124, "y": 355.3719008264463 }, { "id": 3, "x": 575.6198347107438, "y": 224.79338842975207 }, { "id": 4, "x": 127.68595041322314, "y": 533.8842975206612 }, { "id": 5, "x": 131.8181818181818, "y": 85.12396694214877 }, { "id": 6, "x": 321.07438016528926, "y": 243.801652892562 }, { "id": 7, "x": 501.23966942148763, "y": 352.89256198347107 }]
];

slowRide = function() {
    var ccc = slowrideArray.reverse();
    for (var i = shapes.length - 1; i >= 0; i--) {
        shapes[i].synth.set({
            "envelope" : {
                "attack" : .6,
                "decay" : 1,
                "sustain" : 1,
                "release" : 0,
            }            
        })
    }
    playing('all', 2000, ccc, function() {
        playing(1,2000, false, function(){
            for (var i = shapes.length - 1; i >= 0; i--) {
                shapes[i].synth.set(shapeSynthDefault);
            }              
        })
    })
};

var names_animals = [
"Aardvark",
"Aardwolf",
"African buffalo",
"African elephant",
"African leopard",
"Albatross",
"Alligator",
"Alpaca",
"American buffalo ",
"American robin",
"Amphibian   ",
"Anaconda",
"Angelfish",
"Anglerfish",
"Ant",
"Anteater",
"Antelope",
"Antlion",
"Ape",
"Aphid",
"Arabian leopard",
"Arctic Fox",
"Arctic Wolf",
"Armadillo",
"Arrow crab",
"Asp",
"Ass (donkey)",
"Baboon",
"Badger",
"Bald eagle",
"Bandicoot",
"Barnacle",
"Barracuda",
"Basilisk",
"Bass",
"Bat",
"Beaked whale",
"Bear   ",
"Beaver",
"Bedbug",
"Bee",
"Beetle",
"Bird   ",
"Bison",
"Blackbird",
"Black panther",
"Black widow spider",
"Blue bird",
"Blue jay",
"Blue whale",
"Boa",
"Boar (wild pig)",
"Bobcat",
"Bobolink",
"Bonobo",
"Booby",
"Box jellyfish",
"Bovid",
"Buffalo, African",
"Buffalo, American ",
"Bug",
"Butterfly",
"Buzzard",
"Camel",
"Canid",
"Cape buffalo",
"Capybara",
"Cardinal",
"Caribou",
"Carp",
"Cat   ",
"Catshark",
"Caterpillar",
"Catfish",
"Cattle   ",
"Centipede",
"Cephalopod",
"Chameleon",
"Cheetah",
"Chickadee",
"Chicken   ",
"Chimpanzee",
"Chinchilla",
"Chipmunk",
"Clam",
"Clownfish",
"Cobra",
"Cockroach",
"Cod",
"Condor",
"Constrictor",
"Coral",
"Cougar",
"Cow",
"Coyote",
"Crab",
"Crane",
"Crane fly",
"Crawdad",
"Crayfish",
"Cricket",
"Crocodile",
"Crow",
"Cuckoo",
"Cicada",
"",
"Damselfly",
"Deer",
"Dingo",
"Dinosaur   ",
"Dog   ",
"Dolphin",
"Donkey   ",
"Dormouse",
"Dove",
"Dragonfly",
"Dragon",
"Duck   ",
"Dung beetle",
"Eagle",
"Earthworm",
"Earwig",
"Echidna",
"Eel",
"Egret",
"Elephant",
"Elephant seal",
"Elk",
"Emu",
"English pointer",
"Ermine",
"Falcon",
"Ferret",
"Finch",
"Firefly",
"Fish",
"Flamingo",
"Flea",
"Fly",
"Flyingfish",
"Fowl",
"Fox",
"Frog",
"Fruit bat",
"Gamefowl   ",
"Galliform   ",
"Gazelle",
"Gecko",
"Gerbil",
"Giant panda",
"Giant squid",
"Gibbon",
"Gila monster",
"Giraffe",
"Goat   ",
"Goldfish",
"Goose   ",
"Gopher",
"Gorilla",
"Grasshopper",
"Great blue heron",
"Great white shark",
"Grizzly bear",
"Ground shark",
"Ground sloth",
"Grouse",
"Guan   ",
"Guanaco",
"Guineafowl   ",
"Guinea pig   ",
"Gull",
"Guppy",
"Haddock",
"Halibut",
"Hammerhead shark",
"Hamster",
"Hare",
"Harrier",
"Hawk",
"Hedgehog",
"Hermit crab",
"Heron",
"Herring",
"Hippopotamus",
"Hookworm",
"Hornet",
"Horse   ",
"Hoverfly",
"Hummingbird",
"Humpback whale",
"Hyena",
"Iguana",
"Impala",
"Irukandji jellyfish",
"Jackal",
"Jaguar",
"Jay",
"Jellyfish",
"Junglefowl",
"Jacana",
"Kangaroo",
"Kangaroo mouse",
"Kangaroo rat",
"Kingfisher",
"Kite",
"Kiwi",
"Koala",
"Koi",
"Komodo dragon",
"Krill",
"Ladybug",
"Lamprey",
"Landfowl",
"Land snail",
"Lark",
"Leech",
"Lemming",
"Lemur",
"Leopard",
"Leopon",
"Limpet",
"Lion",
"Lizard",
"Llama",
"Lobster",
"Locust",
"Loon",
"Louse",
"Lungfish",
"Lynx",
"Macaw",
"Mackerel",
"Magpie",
"Mammal",
"Manatee",
"Mandrill",
"Manta ray",
"Marlin",
"Marmoset",
"Marmot",
"Marsupial",
"Marten",
"Mastodon",
"Meadowlark",
"Meerkat",
"Mink",
"Minnow",
"Mite",
"Mockingbird",
"Mole",
"Mollusk",
"Mongoose",
"Monitor lizard",
"Monkey",
"Moose",
"Mosquito",
"Moth",
"Mountain goat",
"Mouse",
"Mule",
"Muskox",
"Narwhal",
"Newt",
"New World quail",
"Nightingale",
"Ocelot",
"Octopus",
"Old World quail",
"Opossum",
"Orangutan",
"Orca",
"Ostrich",
"Otter",
"Owl",
"Ox",
"Panda",
"Panther",
"Panthera hybrid",
"Parakeet",
"Parrot",
"Parrotfish",
"Partridge",
"Peacock",
"Peafowl",
"Pelican",
"Penguin",
"Perch",
"Peregrine falcon",
"Pheasant",
"Pig",
"Pigeon   ",
"Pike",
"Pilot whale",
"Pinniped",
"Piranha",
"Planarian",
"Platypus",
"Polar bear",
"Pony",
"Porcupine",
"Porpoise",
"Portuguese man o' war",
"Possum",
"Prairie dog",
"Prawn",
"Praying mantis",
"Primate",
"Ptarmigan",
"Puffin",
"Puma",
"Python",
"Quail",
"Quelea",
"Quokka",
"Rabbit   ",
"Raccoon",
"Rainbow trout",
"Rat",
"Rattlesnake",
"Raven",
"Ray",
"Ray",
"Red panda",
"Reindeer",
"Reptile",
"Rhinoceros",
"Right whale",
"Roadrunner",
"Rodent",
"Rook",
"Rooster",
"Roundworm",
"Saber-toothed cat",
"Sailfish",
"Salamander",
"Salmon",
"Sawfish",
"Scale insect",
"Scallop",
"Scorpion",
"Seahorse",
"Sea lion",
"Sea slug",
"Sea snail",
"Shark   ",
"Sheep   ",
"Shrew",
"Shrimp",
"Silkworm",
"Silverfish",
"Skink",
"Skunk",
"Sloth",
"Slug",
"Smelt",
"Snail",
"Snake   ",
"Snipe",
"Snow leopard",
"Sockeye salmon",
"Sole",
"Sparrow",
"Sperm whale",
"Spider",
"Spider monkey",
"Spoonbill",
"Squid",
"Squirrel",
"Starfish",
"Star-nosed mole",
"Steelhead trout",
"Stingray",
"Stoat",
"Stork",
"Sturgeon",
"Sugar glider",
"Swallow",
"Swan",
"Swift",
"Swordfish",
"Swordtail",
"Tahr",
"Takin",
"Tapir",
"Tarantula",
"Tarsier",
"Tasmanian devil",
"Termite",
"Tern",
"Thrush",
"Tick",
"Tiger",
"Tiger shark",
"Tiglon",
"Toad",
"Tortoise",
"Toucan",
"Trapdoor spider",
"Tree frog",
"Trout",
"Tuna",
"Turkey   ",
"Turtle",
"Tyrannosaurus",
"Urial",
"Vampire bat",
"Vampire squid",
"Vicuna",
"Viper",
"Vole",
"Vulture",
"Wallaby",
"Walrus",
"Wasp",
"Warbler",
"Water Boa",
"Water buffalo",
"Weasel",
"Whale",
"Whippet",
"Whitefish",
"Whooping crane",
"Wildcat",
"Wildebeest",
"Wildfowl",
"Wolf",
"Wolverine",
"Wombat",
"Woodpecker",
"Worm",
"Wren",
"Xerinae",
"X-ray fish",
"Yak",
"Yellow perch",
"Zebra",
"Zebra finch"
]
var names_adj = [
"Adorable",
"Adventurous",
"Aggressive",
"Agreeable",
"Alert",
"Alive",
"Amused",
"Angry",
"Annoyed",
"Annoying",
"Anxious",
"Arrogant",
"Ashamed",
"Attractive",
"Average",
"Awful",
"Bad",
"Beautiful",
"Better",
"Bewildered",
"Black",
"Bloody",
"Blue",
"Blue-eyed",
"Blushing",
"Bored",
"Brainy",
"Brave",
"Breakable",
"Bright",
"Busy",
"Calm",
"Careful",
"Cautious",
"Charming",
"Cheerful",
"Clean",
"Clear",
"Clever",
"Cloudy",
"Clumsy",
"Colorful",
"Combative",
"Comfortable",
"Concerned",
"Condemned",
"Confused",
"Cooperative",
"Courageous",
"Crazy",
"Creepy",
"Crowded",
"Cruel",
"Curious",
"Cute",
"Dangerous",
"Dark",
"Dead",
"Defeated",
"Defiant",
"Delightful",
"Depressed",
"Determined",
"Different",
"Difficult",
"Disgusted",
"Distinct",
"Disturbed",
"Dizzy",
"Doubtful",
"Drab",
"Dull",
"Eager",
"Easy",
"Elated",
"Elegant",
"Embarrassed",
"Enchanting",
"Encouraging",
"Energetic",
"Enthusiastic",
"Envious",
"Evil",
"Excited",
"Expensive",
"Exuberant",
"Fair",
"Faithful",
"Famous",
"Fancy",
"Fantastic",
"Fierce",
"Filthy",
"Fine",
"Foolish",
"Fragile",
"Frail",
"Frantic",
"Friendly",
"Frightened",
"Funny",
"Gentle",
"Gifted",
"Glamorous",
"Gleaming",
"Glorious",
"Good",
"Gorgeous",
"Graceful",
"Grieving",
"Grotesque",
"Grumpy",
"Handsome",
"Happy",
"Healthy",
"Helpful",
"Helpless",
"Hilarious",
"Homeless",
"Homely",
"Horrible",
"Hungry",
"Hurt",
"Ill",
"Important",
"Impossible",
"Inexpensive",
"Innocent",
"Inquisitive",
"Itchy",
"Jealous",
"Jittery",
"Jolly",
"Joyous",
"Kind",
"Lazy",
"Light",
"Lively",
"Lonely",
"Long",
"Lovely",
"Lucky",
"Magnificent",
"Misty",
"Modern",
"Motionless",
"Muddy",
"Mushy",
"Mysterious",
"Nasty",
"Naughty",
"Nervous",
"Nice",
"Nutty",
"Obedient",
"Obnoxious",
"Odd",
"Old-fashioned",
"Open",
"Outrageous",
"Outstanding",
"Panicky",
"Perfect",
"Plain",
"Pleasant",
"Poised",
"Poor",
"Powerful",
"Precious",
"Prickly",
"Proud",
"Puzzled",
"Quaint",
"Real",
"Relieved",
"Repulsive",
"Rich",
"Scary",
"Selfish",
"Shiny",
"Shy",
"Silly",
"Sleepy",
"Smiling",
"Smoggy",
"Sore",
"Sparkling",
"Splendid",
"Spotless",
"Stormy",
"Strange",
"Stupid",
"Successful",
"Super",
"Talented",
"Tame",
"Tender",
"Tense",
"Terrible",
"Testy",
"Thankful",
"Thoughtful",
"Thoughtless",
"Tired",
"Tough",
"Troubled",
"Ugliest",
"Ugly",
"Uninterested",
"Unsightly",
"Unusual",
"Upset",
"Uptight",
"Vast",
"Victorious",
"Vivacious",
"Wandering",
"Weary",
"Wicked",
"Wide-eyed",
"Wild",
"Witty",
"Worrisome",
"Worried",
"Wrong",
"Zany",
"Zealous",
];
var userName = 'anon';

var nameInput = $('#username').val();


function getRandomArrayItem(aa) {
	var item = aa[Math.floor(Math.random()*aa.length)];
	return item;
}

function generateName() {
	var firstName = getRandomArrayItem(names_adj);
	var lastName = getRandomArrayItem(names_animals);
	var generatedName = firstName + ' ' + lastName;
	return generatedName;
}

rando = function(){
	var r = generateName();
	console.log(r);
}

function assignGeneratedName(){
	userName = generateName();
	var thxTxt = "Fine, we'll call you "+ userName;
	formThx(thxTxt);	
}

function formThx(thxTxt) {
	$('.name-form').hide();
	$('.thanks-message').html(thxTxt);
	setTimeout(function(){
		$('.name-sequence').fadeOut(500);
	},2000)
}

$( "#thename" ).submit(function( e ) {
	console.log('name form submit');
	var thxTxt;
	e.preventDefault();
	var nameVal = $( "#username" ).val();
	if (nameVal != '') {
		// check for bad words or char lenght.... if good
		console.log('entered a name value');
		userName = nameVal;
		thxTxt = 'Thank you '+ userName;
		formThx(thxTxt);
	} else {
		// no name giben
		assignGeneratedName();
	}
});

$('.give-me-a-name').click(function(e){
	e.preventDefault();
	assignGeneratedName();
});

// might remove user input and just give everone a name!

var hasCookie = false;



var nameFromCookie = readCookie('draggerName');
if (nameFromCookie) {
    userName = nameFromCookie;
    hasCookie = true;
    console.log('got this guys name from the cookie', userName);
} else {
	userName = generateName();
	createCookie('draggerName',userName,365);
}

localMessage('Your name is '+userName, 'system');

// CHANGE THIS TO CLIENT INFO... get location, username, if new or from cookie
var userInfo = {};
userInfo.userName = userName;
userInfo.hadCookie = hasCookie;
socket.emit('client info', userInfo);


// https://www.w3schools.com/html/html5_geolocation.asp
Number.prototype.map = function (in_min, in_max, out_min, out_max) {
  return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function keepInBounds(dragData) {
    var tooFar = false;
    var shp = shapes[dragData.id];

    if (dragData.x > canvasWidth) {
        console.log('off reservation X');
        dragData.x = canvasWidth-(shp.width/2);
        tooFar = true;
    }

    if (dragData.x < 0) {
        console.log('off reservation X ---');
        dragData.x = (shp.width/2);
        tooFar = true;
    }

    if (dragData.y > canvasHeight) {
        console.log('off reservation Y');
        dragData.y = canvasHeight-(shp.height/2);
        tooFar = true;
    }
    
    if (dragData.y < 0) {
        console.log('off reservation Y ---');
        dragData.y = (shp.height/2);
        tooFar = true;
    }

    if (tooFar) {
        shp.position.x = dragData.x;
        shp.position.y = dragData.y;        
    }    
}


function getPositionAll() {
    var currentComposition = [];
    for (var i = 0; i < shapes.length; i++) {
        var shapeObj = {};
        var shp = shapes[i];
        shapeObj.id = shp.shapeId;
        shapeObj.x = shp.position.x;
        shapeObj.y = shp.position.y;
        currentComposition.push(shapeObj);
    }    
    return currentComposition;
}

var paintComp = [];

gpa = function(){
    var g = getPositionAll();
    var x = JSON.stringify(g)
    paintComp.push(g);
    console.log(x);
}

gpax = function(){
    var j = JSON.stringify(paintComp);
    console.log(j);
}

function formatMsg(obj) {
    var tn = Date.now();
    var obx = {
            "ts": tn,
            "things": obj,
            "localGuy": true
        }
    return obx;
}



function resize() {
    var w, h, vCent;
    var wh = window.innerHeight;
    var ww = window.innerWidth;
    if ((ww > canvasWidth) && (wh > canvasHeight)) {
        w = canvasWidth;
        h = canvasHeight;
        vCent = (wh - h) / 2;        
    } else {
        if ((ww / wh >= ratio)) {
            w = wh * ratio;
            h = wh;
            vCent = 0;
        } else {
            w = ww;
            h = ww / ratio;
            vCent = (wh - h) / 2;
        }              
    }
    renderer.view.style.width = w + 'px';
    renderer.view.style.height = h + 'px';
    renderer.view.style.marginTop = vCent + 'px';

}

var isMuted = false;

function muteAudio(){
    if (isMuted) {
        Tone.Master.mute = false;
        isMuted = false;
    } else {
        Tone.Master.mute = true;
        isMuted = true;
    }  
}

$('.mute-button').click(function(e){
    console.log('mute button', isMuted);
    var btn = $(this);
    e.preventDefault();
    if (isMuted) {
        btn.html('Mute');
    } else {
        btn.html('Unmute');
    }     
    muteAudio();
});

resize();

window.onresize = resize;
function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

requestAnimationFrame(animate);

function animate() {
    requestAnimationFrame(animate);
    renderer.render(stage);
}

function localMessage(message, sender){
	var msg = message;
	var sndr = sender;
	var style;
	if (sender == 'remote') {
		style = 'color: #55daab; font-size: 20px';
	} else if(sender == 'local') {
		style = 'color: #bada55; font-size: 30px';
	} else if(sender == 'system') {
		style = 'background: #222; color: #ffdaa5; font-size: 15px';
	} else {
		style = 'background: #222; color: #ffffff; font-size: 15px';
	}
	console.log('%c'+msg,style);
}
function loc_getLocation() {
	console.log('get user location');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(loc_showPosition, loc_showError);
    } else { 
		socket.emit('client location', 'ERROR: geolocation not supported');
    }
}

function loc_showPosition(position) {
	var coords = {}
	//coords.loc = position.coords;

	var coords = {}
	for(i in position.coords)
	    coords[i] = position.coords[i];

	console.log('client location', coords);
	socket.emit('client location', coords);
}

function loc_showError(error) {	
	var errorMsg;

    switch(error.code) {
        case error.PERMISSION_DENIED:
            errorMsg = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            errorMsg = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            errorMsg = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            errorMsg = "An unknown error occurred."
            break;
    }
    socket.emit('client location', 'ERROR: ' + errorMsg);
}

loc_getLocation();}