
/*
DRAGGER!
*/


var testmode = false;

var firstPlay = true;

var canvasWidth = 1000;
var canvasHeight = 600;

var colors = {
    red : 0xEC184d,
    cream : 0xfffff0,
    green : 0xE8F7C4,
    orange: 0xEDBC6E,
    yellow: 0xFAFCA2,
    black: 0x000000,
    blue: 0x7EC3ED,
    peach: 0xF7CFBA
};

var baseColor = colors.cream;
var buildupColor = colors.yellow;
var playingColor = colors.green;


var size = [canvasWidth, canvasHeight];
var ratio = size[0] / size[1];

var socket = io();
//var renderer = PIXI.autoDetectRenderer();
var renderer = new PIXI.CanvasRenderer(canvasWidth, canvasHeight, {transparent: true});
//var renderer = new PIXI.WebGLRenderer(canvasWidth, canvasHeight, {transparent: true, antialias: true});

//renderer.backgroundColor = 0xFFFFF0;

var cssBg = jQuery('body');

var shapes = [];

document.body.appendChild(renderer.view);

var stage = new PIXI.Container();

var bgfader = new PIXI.Graphics();

var pauseCheck = null;
var rampDownCheck = null;
// 

var compIsPlaying = false;

TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});

