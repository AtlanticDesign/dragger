var arpSyn;
var arpIntoduced = false;
var notesUp = [
    'D5',
    'G6',
    'C5',
    'D6',
    'G5',
    'C6',
    'D6',
    'G4'
]

function setUpArp() {
	arpSyn = new Tone.Synth({
        "oscillator" : {
            "type" : "square"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : 1.2,
            "decay" : 1,
            "sustain" : 1,
            "release" : 1.2,
        }		
	});

	var pattern = new Tone.Pattern(function(time, note){
	    arpSyn.triggerAttackRelease(note, 0.25);
	}, notes, "random");

	pattern.start(0);
	Tone.Transport.start();
    arpSyn.volume.value = -10000; 
    arpSyn.chain(sDelay, dsVol, Tone.Master);




    arpSyn2 = new Tone.Synth({
        "oscillator" : {
            "type" : "sine"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : .1,
            "decay" : 1,
            "sustain" : 1,
            "release" : .1,
        }       
    });

    var pattern2 = new Tone.Pattern(function(time, note){
        arpSyn2.triggerAttackRelease(note, 0.05);
    }, notesUp, "random");
   // pattern2.interval = 2;
    pattern2.playbackRate = 3.3;
    pattern2.humanize = true;
    pattern2.start(.2);
    //Tone.Transport.start();
    arpSyn2.volume.value = -48; 
    arpSyn2.chain(sDelay, dsVol, Tone.Master);    
}

setUpArp();