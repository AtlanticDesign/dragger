
var jahSprites = [];
var jahColors = [
	0xFF0000,
	0xFFFF00,
	0x00FF00,
	0x00FFFF,
	0xFF700B
];

var jahColorNo = 0;


function jahCray() {


    for (var i = shapes.length - 1; i >= 0; i--) {

        if (jahColorNo < jahColors.length -1) {
            jahColorNo ++;
        } else {
            jahColorNo = 0;
        }     

		var jahGraph = new PIXI.Graphics();
		jahGraph.beginFill(jahColors[jahColorNo]);
		jahGraph.drawRect(0, 0, canvasWidth, canvasHeight);
		jahGraph.mask = shapes[i];
		stage.addChild(jahGraph);
        jahSprites.push(jahGraph);   
    }  



	TweenMax.to(cssBg, .3, {backgroundColor:'#000000', ease:Linear.easeNone});  
	playing(100, 0, false, false);
	setTimeout(function(){
		playing(100, 0, false, function(){
			for (var i = jahSprites.length - 1; i >= 0; i--) {
				jahSprites[i].mask = null;
				stage.removeChild(jahSprites[i]);
			}
			endPlaying();			
		});
	},1500);      
}

jCray = function(){
	jahCray();
}