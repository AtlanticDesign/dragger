var userMoves = 0;
function onDragEnd(event) {
    // CLENT DRAG END
    var dragData = {};

    imDragging = false;
    
    dragData.id = this.shapeId;
    dragData.x = this.position.x;
    dragData.y = this.position.y;

    keepInBounds(dragData); 
    
    this.alpha = 1;
    this.dragging = false;
    this.data = null;

    var posAll = getPositionAll();

    var dataForServer = {};
    dataForServer.dragData = dragData;
    dataForServer.posAll = posAll;

    socket.emit('drag end', dataForServer);

    TweenMax.to(cssBg, 2, {backgroundColor:buildupColor, delay:1, ease:Linear.easeNone});
 
    this.synth.triggerRelease();
    
    startPlayingTimer();
    if (hasData) {
        fdataSort.unshift(formatMsg(posAll));
    }
    userMoves++;
    if (userMoves == 1) {
        localMessage('You just made your first move', 'local');
    } else {
        localMessage('You have made '+userMoves+' moves so far', 'local');
    }
}


var tempData = false;

socket.on('update drag', function(msg) {
    if (gotInitialData) {
        // REMOTE DRAG END
        console.log(msg);
        var id = msg.dragData.id;
        var sprt = shapes[id];
        //keepInBounds(msg.dragData); 
        sprt.position.x = msg.dragData.x;
        sprt.position.y = msg.dragData.y;

        sprt.scale.set(.5);

        sprt.remoteDragger = false;    
        sprt.interactive = true;    
        sprt.alpha = 1;

        TweenMax.to(cssBg, 2, {backgroundColor:buildupColor, delay:1, ease:Linear.easeNone});
        
        setTimeout(function(){
            sprt.verb.wet.rampTo(0, .3);        
            sprt.synth.volume.rampTo(-30, .3); 
        },300);
           
        sprt.synth.triggerRelease();

        startPlayingTimer();

        fdataSort.unshift(formatMsg(msg.posAll));
    }
});


function startPlayingTimer() {
    clearPlayingTimer();
    pauseCheck = setTimeout(function() {
        playing(30, 100, false, endPlaying);
        TweenMax.to(cssBg, .3, {backgroundColor:playingColor, ease:Linear.easeNone});        
        pauseCheck = null;
    }, 3000);

    // AUDIO //

    rampDownCheck = setTimeout(function(){
        dsVol.volume.rampTo(bgVolDown, 3);
        rampDownCheck = null;
    },1200)    
}


function endPlaying() {
    TweenMax.to(cssBg, .2, {backgroundColor:baseColor, ease:Linear.easeNone, onComplete:function(){
        dsVol.volume.rampTo(bgVolUp, .4);
        compIsPlaying = false;

        if (!arpIntoduced) {
            setTimeout(function(){
                arpSyn.volume.rampTo(-23, 3); 
            },1400);
            arpIntoduced = true;
        }   
    }});    
}