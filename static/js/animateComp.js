playing = function(numb, intr, sequence, callback){
    compIsPlaying = true;
    
    var curIndex;
    var xx = [];
    var aniInterval;
  
    if (intr) {
        aniInterval = intr
    } else {
        aniInterval = 100
    }

    var aniIntervalSecs = aniInterval / 1000;
    
    if (numb == 'all') {

        if (sequence) {
            curIndex = sequence.length;
        } else {
            if (hasData) {
                curIndex = fdataSort.length;
            } else {
                curIndex = 10;
            }
        }


    } else if (numb != undefined) {
        curIndex = numb;
    } else {
        curIndex = 10;
        if (testmode) {
            curIndex = 5;
        }
    }

    for (var i = 0; i < shapes.length; i++) {
        var shp = shapes[i];
        shp.interactive = false;
    }    


    
    function advanceComp() {
        --curIndex;
        if (curIndex >= 0) {
            //console.log('index', curIndex, fdataSort.length);

            /// COPY PASTE YO
            var comp;

            if (sequence) {
                comp = sequence[curIndex];
            } else {
                if (hasData) {
                    comp = fdataSort[curIndex].things;

                } else {
                    comp = chillData;
                }
            }
            xx.push(comp);
            for (var i = 0; i < comp.length; i++)
            {
                var key = comp[i];
                var sprt = shapes[key.id];
                sprt.alpha = 1;

                var hasMoved = false;
                if (key.x == sprt.position.x && key.y == sprt.position.y) {

                } else {
                    hasMoved = true;
                    var objData = {
                        id : key.id,
                        x : key.x,
                        y : key.y,
                        hasMoved : hasMoved
                    }
                    updateSound(objData, true, aniIntervalSecs);

                                       
                    // var line = new PIXI.Graphics();

                    // line.lineStyle(1, colors.red, .5);
                    // line.moveTo(sprt.position.x, sprt.position.y);
                    // line.lineTo(key.x, key.y);
                    // linesCont.addChild(line);
                    // sprt.lines.push(line);
                    // if (sprt.lines.length > 2) {
                    //     var lastLine = sprt.lines.shift();
                    //     linesCont.removeChild(lastLine);
                    // }   


                }
                TweenMax.to(sprt.position, (aniIntervalSecs), {x:key.x , y:key.y});

            }



           setTimeout(advanceComp, aniInterval);
        } else {
            firstPlay = false;
            curIndex = false;
            if (callback) {
                callback();    
            }
            var xy = JSON.stringify(xx)
            //console.log(xy);

            for (var i = 0; i < shapes.length; i++) {
                var shp = shapes[i];
                shp.interactive = true; 
            }
                
        }
    }

    advanceComp();
}

removeLines = function() {
    for (var i = shapes.length - 1; i >= 0; i--) {
        for (var ii = shapes[i].lines.length - 1; ii >= 0; ii--) {
            linesCont.removeChild(shapes[i].lines[ii]);
        }
    }    
}

tint = function(){
    for (var i = shapes.length - 1; i >= 0; i--) {
        for (var ii = shapes[i].lines.length - 1; ii >= 0; ii--) {
            shapes[i].lines[ii].tint = 0x000000;
        }
    }
}