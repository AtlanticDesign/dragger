var ds1, ds2, ds3, ds4;
var dSFilter, dsLfo, sDelay, dsVol, dReverb, droneVol;

function setupDroneSynths() {
    ds1 = new Tone.Synth({
        "oscillator" : {
            "type" : "sine"
        },
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    });    

    ds2 = new Tone.Synth({
        "oscillator" : {
            "type" : "triangle"
        },
        //"detune" : -700,
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    }); 

    ds3 = new Tone.Synth({
        "oscillator" : {
            "type" : "sine"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    }); 

    ds4 = new Tone.Synth({
        "oscillator" : {
            "type" : "square"
        },
        //"detune" : -1400,
        "envelope" : {
            "attack" : 10,
            "decay" : 1,
            "sustain" : 1,
            "release" : 100.2,
        }
    }); 

}



function wireDroneSynths() {

    ds1.chain(droneVol);

    ds2.chain(droneVol);

    ds3.chain(droneVol);

    ds4.chain(dSFilter, sDelay, dReverb, droneVol);

    droneVol.connect(dsVol);

}





function playDroneSynths() {
    setupDroneSynths();
    wireDroneSynths();

    ds1.volume.value = -30; 
    ds1.triggerAttack('G2');

    ds2.volume.value = -60; 
    ds2.triggerAttack('B6');

    ds3.volume.value = -20; 
    ds3.triggerAttack('G3');

    //ds4.connect(dSFilter);
    ds4.volume.value = -45; 
    ds4.triggerAttack('D3');

}


playDroneSynths();
