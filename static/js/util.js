Number.prototype.map = function (in_min, in_max, out_min, out_max) {
  return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function keepInBounds(dragData) {
    var tooFar = false;
    var shp = shapes[dragData.id];

    if (dragData.x > canvasWidth) {
        console.log('off reservation X');
        dragData.x = canvasWidth-(shp.width/2);
        tooFar = true;
    }

    if (dragData.x < 0) {
        console.log('off reservation X ---');
        dragData.x = (shp.width/2);
        tooFar = true;
    }

    if (dragData.y > canvasHeight) {
        console.log('off reservation Y');
        dragData.y = canvasHeight-(shp.height/2);
        tooFar = true;
    }
    
    if (dragData.y < 0) {
        console.log('off reservation Y ---');
        dragData.y = (shp.height/2);
        tooFar = true;
    }

    if (tooFar) {
        shp.position.x = dragData.x;
        shp.position.y = dragData.y;        
    }    
}


function getPositionAll() {
    var currentComposition = [];
    for (var i = 0; i < shapes.length; i++) {
        var shapeObj = {};
        var shp = shapes[i];
        shapeObj.id = shp.shapeId;
        shapeObj.x = shp.position.x;
        shapeObj.y = shp.position.y;
        currentComposition.push(shapeObj);
    }    
    return currentComposition;
}

var paintComp = [];

gpa = function(){
    var g = getPositionAll();
    var x = JSON.stringify(g)
    paintComp.push(g);
    console.log(x);
}

gpax = function(){
    var j = JSON.stringify(paintComp);
    console.log(j);
}

function formatMsg(obj) {
    var tn = Date.now();
    var obx = {
            "ts": tn,
            "things": obj,
            "localGuy": true
        }
    return obx;
}



function resize() {
    var w, h, vCent;
    var wh = window.innerHeight;
    var ww = window.innerWidth;
    if ((ww > canvasWidth) && (wh > canvasHeight)) {
        w = canvasWidth;
        h = canvasHeight;
        vCent = (wh - h) / 2;        
    } else {
        if ((ww / wh >= ratio)) {
            w = wh * ratio;
            h = wh;
            vCent = 0;
        } else {
            w = ww;
            h = ww / ratio;
            vCent = (wh - h) / 2;
        }              
    }
    renderer.view.style.width = w + 'px';
    renderer.view.style.height = h + 'px';
    renderer.view.style.marginTop = vCent + 'px';

}

var isMuted = false;

function muteAudio(){
    if (isMuted) {
        Tone.Master.mute = false;
        isMuted = false;
    } else {
        Tone.Master.mute = true;
        isMuted = true;
    }  
}

$('.mute-button').click(function(e){
    console.log('mute button', isMuted);
    var btn = $(this);
    e.preventDefault();
    if (isMuted) {
        btn.html('Mute');
    } else {
        btn.html('Unmute');
    }     
    muteAudio();
});

resize();

window.onresize = resize;