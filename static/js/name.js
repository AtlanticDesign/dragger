var userName = 'anon';

var nameInput = $('#username').val();


function getRandomArrayItem(aa) {
	var item = aa[Math.floor(Math.random()*aa.length)];
	return item;
}

function generateName() {
	var firstName = getRandomArrayItem(names_adj);
	var lastName = getRandomArrayItem(names_animals);
	var generatedName = firstName + ' ' + lastName;
	return generatedName;
}

rando = function(){
	var r = generateName();
	console.log(r);
}

function assignGeneratedName(){
	userName = generateName();
	var thxTxt = "Fine, we'll call you "+ userName;
	formThx(thxTxt);	
}

function formThx(thxTxt) {
	$('.name-form').hide();
	$('.thanks-message').html(thxTxt);
	setTimeout(function(){
		$('.name-sequence').fadeOut(500);
	},2000)
}

$( "#thename" ).submit(function( e ) {
	console.log('name form submit');
	var thxTxt;
	e.preventDefault();
	var nameVal = $( "#username" ).val();
	if (nameVal != '') {
		// check for bad words or char lenght.... if good
		console.log('entered a name value');
		userName = nameVal;
		thxTxt = 'Thank you '+ userName;
		formThx(thxTxt);
	} else {
		// no name giben
		assignGeneratedName();
	}
});

$('.give-me-a-name').click(function(e){
	e.preventDefault();
	assignGeneratedName();
});

// might remove user input and just give everone a name!

var hasCookie = false;



var nameFromCookie = readCookie('draggerName');
if (nameFromCookie) {
    userName = nameFromCookie;
    hasCookie = true;
    console.log('got this guys name from the cookie', userName);
} else {
	userName = generateName();
	createCookie('draggerName',userName,365);
}

localMessage('Your name is '+userName, 'system');

// CHANGE THIS TO CLIENT INFO... get location, username, if new or from cookie
var userInfo = {};
userInfo.userName = userName;
userInfo.hadCookie = hasCookie;
socket.emit('client info', userInfo);


// https://www.w3schools.com/html/html5_geolocation.asp