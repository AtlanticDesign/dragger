function localMessage(message, sender){
	var msg = message;
	var sndr = sender;
	var style;
	if (sender == 'remote') {
		style = 'color: #55daab; font-size: 20px';
	} else if(sender == 'local') {
		style = 'color: #bada55; font-size: 30px';
	} else if(sender == 'system') {
		style = 'background: #222; color: #ffdaa5; font-size: 15px';
	} else {
		style = 'background: #222; color: #ffffff; font-size: 15px';
	}
	console.log('%c'+msg,style);
}