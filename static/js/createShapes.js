var linesCont = new PIXI.Container();
var shapesCont = new PIXI.Container();
stage.addChild(linesCont);


var lineColors = [
    0xFF0000,
    0xFFFF00,
    0x00FF00,
    0x00FFFF,
    0xFF700B,
    0x00FFBB,
    0xF0FFF0,
    0xDDFFA7,
    0xFF00FF
];

var spriteNames = [
    'jerry'
];

var circNo = 1, barNo = 1, triNo = 1, rectNo = 1;


function createShape(x, y, i) {

    var bxId = i;

    if (noteNo < notes.length -1) {
        noteNo ++;
    } else {
        noteNo = 0;
    }


    if (texNo < textures.length -1) {
        texNo ++;
    } else {
        texNo = 0;
    }

    var shape = new PIXI.Sprite(textures[texNo]);

    shape.anchor.set(0.5);

    shape.scale.set(.5);


    // var shape = new PIXI.Graphics();


    // shape.lineStyle(0);
    // shape.beginFill(colors.red, 0.5);
    // shape.drawCircle(0,0,60,60);
    // shape.endFill();
    // shape.cacheAsBitmap = true;

    shape.lines = [];

    shape.interactive = false;

    shape.buttonMode = true;
    shape.defaultCursor = "move"; 

    if (!testmode) {
        shape.alpha = 0;
    }
    shape.shapeId = i;
    shape.note = notes[noteNo];
    shape.newNote = false;
    shape
        .on('mousedown', onDragStart)
        .on('touchstart', onDragStart)
        .on('mouseup', onDragEnd)
        .on('mouseupoutside', onDragEnd)
        .on('touchend', onDragEnd)
        .on('touchendoutside', onDragEnd)
        .on('mousemove', onDragMove)
        .on('touchmove', onDragMove);

    shape.position.x = x;
    shape.position.y = y;

    if (textures[texNo] == bar) {
        shape.rotation = 0.59009582;
        shape.funName = 'bar ' + barNo;
        barNo ++;
    } else if (textures[texNo] == circ) {
        shape.funName = 'circle ' + circNo;
        circNo ++;
    } else if (textures[texNo] == rect) {
        shape.funName = 'rectangle ' + rectNo;
        rectNo ++;
    } else if (textures[texNo] == tri) {    
        shape.funName = 'triangle ' + triNo;
        triNo ++;    
    }

    // AUDIO //
    setupAudio(shape);
    
    // add it to the stage
    shapesCont.addChild(shape);
    shapes.push(shape);
    //shape.mask = myMask
}





//shapesCont.addChild(blaSprite);
//blajam.addChild(shapesCont);
//stage.addChild(blajam);

for (var i = 0; i < 8; i++) {
    createShape(10*i+1 , 10*i+1, i);
}

stage.addChild(shapesCont);
//var blajam = new PIXI.Container();



