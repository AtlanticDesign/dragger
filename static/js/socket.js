var gotInitialData = false;
var serverSaysAnimating = false;

socket.emit('CLIENT get comp');

socket.on('SERVER send comp', function(data) {
    console.log('data from server', data.Items.length);
    // GET ALL FROM DB AND SORT
    var fdata = data.Items;
    fdataSort = fdata.sort(function(a, b) {
        return parseFloat(b.ts) - parseFloat(a.ts);
    });
    console.log(JSON.stringify(fdataSort));
    gotInitialData = true;

    var sprts = [];
    // POSITION THE COMPOSITION
    for (var i = 0; i < comp.length; i++)
    {
        var key = comp[i];
        var sprt = shapes[key.id];
        sprt.interactive = true;

        sprt.position.x = key.x;
        sprt.position.y = key.y;
        sprts.push(sprt);


        var objData = {
            id : key.id,
            x : key.x,
            y : key.y
        }
        updateSound(objData); 
        keepInBounds(objData); 
    }
    var timer = 100;
  
    setTimeout(function(){
        TweenMax.staggerTo(sprts, .4, {alpha:1},.3);
    },timer);

});
