// create a texture from an image path
var texture = PIXI.Texture.fromImage('/static/img/drag/bunny.png');
var circ = PIXI.Texture.fromImage('/static/img/drag/circ.png');
var rect = PIXI.Texture.fromImage('/static/img/drag/rect.png');
var tri = PIXI.Texture.fromImage('/static/img/drag/tri.png');
var bar = PIXI.Texture.fromImage('/static/img/drag/bar.png');

var textures = [circ,rect,tri,bar];
//console.log('textures length ' + textures.length);

var notes = [
	'D3',
	'G3',
	'C3',
	'D4',
	'G4',
	'C4',
	'D5',
	'G5'
]

var imDragging = false;
var isPlaying = false;
var fdataSort;
var hasData = false;

var bxId = 0;
    bxId = 1000;

var texNo = 0;
var noteNo = 0;

var bgVolUp = -10;
var bgVolDown = -30;

var chillData = [
	{
		id: 0,
		x: 100,
		y: 100
	},
	{
		id: 1,
		x: 100,
		y: 100
	},
	{
		id: 2,
		x: 100,
		y: 100
	},
	{
		id: 3,
		x: 100,
		y: 100
	},
	{
		id: 4,
		x: 100,
		y: 100
	},
	{
		id: 5,
		x: 100,
		y: 100
	},
	{
		id: 6,
		x: 100,
		y: 100
	},
	{
		id: 7,
		x: 100,
		y: 100
	},
];