function onDragStart(event) {
    var sprt = this;

    function okToDrag(sprt){
        console.log('ok to drag', sprt);
        dsVol.volume.rampTo(bgVolUp, .3);
        TweenMax.killTweensOf(cssBg);    
        TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});


        imDragging = sprt.shapeId;    
        sprt.data = event.data;
        sprt.alpha = 1;
        sprt.dragging = true;
        //sprt.defaultCursor = "grabbing"; //or some othe css cursor style
                
        socket.emit('drag start', sprt.shapeId);

        sprt.verb.wet.value = 0;
        sprt.synth.triggerAttack(sprt.note);
        clearPlayingTimer();
        localMessage('You are dragging '+sprt.funName, 'local');
    }
    // attempting to prevent mutiple users from grabbing the same shape
    setTimeout(function(){
        if (!sprt.remoteDragger) {
            okToDrag(sprt);
        }
    },200);
}



socket.on('flag drag', function(msg) {
    // REMOTE DRAG START
    console.log('Got the drag start', msg);

    var id = msg.shapeId;
    var sprt = shapes[id];
    sprt.remoteDragger = true;
    // IF someone else is dragging, turn off interactivity;
    if (imDragging != id) {
        sprt.interactive = false;
    }

    dsVol.volume.rampTo(bgVolUp, .3);
    TweenMax.killTweensOf(cssBg);
    TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});

    sprt.verb.wet.value = 1;
    sprt.synth.volume.value = -42;    
    sprt.synth.triggerAttack(sprt.note);

    sprt.alpha = .2;
    sprt.scale.set(.5);
    clearPlayingTimer();
    localMessage(msg.appUserName+' is dragging '+sprt.funName, 'remote');
});



function clearPlayingTimer() {
    clearTimeout(pauseCheck);
    clearTimeout(rampDownCheck);    
}