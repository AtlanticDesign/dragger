function loc_getLocation() {
	console.log('get user location');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(loc_showPosition, loc_showError);
    } else { 
		socket.emit('client location', 'ERROR: geolocation not supported');
    }
}

function loc_showPosition(position) {
	var coords = {}
	//coords.loc = position.coords;

	var coords = {}
	for(i in position.coords)
	    coords[i] = position.coords[i];

	console.log('client location', coords);
	socket.emit('client location', coords);
}

function loc_showError(error) {	
	var errorMsg;

    switch(error.code) {
        case error.PERMISSION_DENIED:
            errorMsg = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            errorMsg = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            errorMsg = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            errorMsg = "An unknown error occurred."
            break;
    }
    socket.emit('client location', 'ERROR: ' + errorMsg);
}

loc_getLocation();