

function onDragMove(event) {
    if (this.dragging) {

        var newPosition = this.data.getLocalPosition(this.parent);
        this.position.x = newPosition.x;
        this.position.y = newPosition.y;

        var dragData = {};
        
        dragData.id = this.shapeId;
        dragData.x = newPosition.x;
        dragData.y = newPosition.y;
        
        socket.emit('drag live', dragData);
        
        updateSound(dragData, false);
        updateDrone(dragData);
    }
}




socket.on('draging live', function(msg) {
   // console.log('draging live', msg);
    if (gotInitialData) {

        var id = msg.id;
        var sprt = shapes[id];

        var dragData = {};
        dragData.id = msg.id;
        dragData.x = msg.x;
        dragData.y = msg.y;

        sprt.position.x = msg.x;
        sprt.position.y = msg.y;

        updateSound(dragData, false);
    }        
});
