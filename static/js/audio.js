function setupEffects(){

    var dsFilterArgs = {
        type:"highpass",
        //frequency:350,
        //rolloff:-12,
        Q:2,
        gain:0        
    }

    dSFilter = new Tone.Filter(dsFilterArgs);

    dsLfo = new Tone.LFO(.1, 400, 4000);
    dsLfo.connect(dSFilter.frequency);
    dsLfo.start();

    sDelay = new Tone.FeedbackDelay(.15, 0);

    droneVol = new Tone.Volume(bgVolUp);

    dsVol = new Tone.Volume(bgVolUp);



   // dsVol.chain(sDelay, Tone.Master);

    dReverb = new Tone.JCReverb(0.4);    
}
setupEffects();


var shapeSynthDefault = {
    "oscillator" : {
        "type" : "square"
    },
    "envelope" : {
        "attack" : .02,
        "decay" : 1,
        "sustain" : 1,
        "release" : 5.2,
    }
}

function setupAudio(shape) {
    shape.synth = new Tone.Synth({
        "oscillator" : {
            "type" : "square"
        },
        "envelope" : {
            "attack" : .02,
            "decay" : 1,
            "sustain" : 1,
            "release" : 5.2,
        }
    }).toMaster();    

    var filterArgs = {
        type:"lowpass",
        frequency:350,
        rolloff:-12,
        Q:10,
        gain:1        
    }

    shape.verb = new Tone.JCReverb(0.7).toMaster();

    shape.verb.wet.value = 0;

    shape.filter = new Tone.Filter(filterArgs).toMaster();

    //shape.synth.connect(shape.filter);

    shape.synth.chain(shape.filter, shape.verb);

    shape.synth.volume.value = -30;    
}

function updateSound(data, attack, intr) {
    if (!intr) {
        intr = .1;
    }

    var obj = shapes[data.id];
    var syn = obj.synth;

    var oNote = obj.note;
    var nNote = changeNote(obj.synth, data.y);

    var vel = 1;

    adjustFilter(obj.filter, data.x);
    
    syn.setNote(nNote);

    //console.log(data.id, oNote, nNote);
    if (oNote == nNote) {
        //console.log('the same note');
        syn.newNote = false;
        vel = .1;
        // syn.volume.value = -60;    

    } else {
        //console.log('DIFFERENT NOTE');
        syn.newNote = true;
       // syn.volume.value = -30;    
    }

    if (attack && data.hasMoved) {
        //console.log('TRIGGER!!');
        syn.triggerAttackRelease(nNote, intr);
    }    


    obj.note = nNote;    
}

function updateDrone(data){
    var d = data.x;
    var x = d.map(0, 1000, 0, 100);
    dReverb.set({gain: data.x});
}


function adjustFilter(filt,pos) {
    var x = pos.map(0, 1000, 0, 500)
    filt.set({frequency: x});
}


function hasNoteChanged(syn) {
    var oldNote = '';
    var newNOte = '';
    if (oldNote == newNote) {
        changed = false;
    } else {
        changed = true;
    }
    return changed;
}

function changeNote(syn, pos){
    var note;
    if (pos >= 0 && pos <= 100) {
        note = "C3"; 
    } else if (pos >= 100 && pos <= 200) {
        note = "D3"; 
    } else if (pos >= 200 && pos <= 300) {
        note = "G3"; 
    } else if (pos >= 300 && pos <= 400) {
        note = "D4"; 
    } else if (pos >= 400 && pos <= 500) {
        note = "G4"; 
    } else if (pos >= 500 && pos <= 600) {
        note = "D5"; 
    } else {
        note = "D7"; 
    } 
    return note;   
}


for (var i = 0; i < shapes.length; i++) {
    var shp = shapes[i];
    //shp.synth.connect(sDelay);
}  


//var masterDelay = new Tone.FeedbackDelay(1, 0);

//Tone.Master.connect(masterDelay);
