var io = require('socket.io');

module.exports = {


    formatMsg: function(obj) {
        var tn = Date.now();
        var obx = {
                "ts": tn,
                "things": obj
            }
        return obx;
    },

    addItemToDb: function(obj,table,dbdc) {
        var params = {
            TableName: table,
            Item: obj
        };

        dbdc.put(params, function(err, data) {
            if (err) {
                console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("THIS SHIT WORKS!!!! Added item:", JSON.stringify(data, null, 2));
            }
        });
    },


    readDb: function(table, command, dbdc, callback) {
        //console.log('readDB',table, command, dbdc, callback);
        var ds;
        var params = {
            TableName: table
        };

        dbdc.scan(params, function(err, data) {
            if (err)
                console.log('DB ERROR!!! ', JSON.stringify(err, null, 2));
            else
                console.log('DB SUCCESS!!! ', data, table);
                ds = data;
                callback();
                if (command) {
                    //io.emit(command, data);
                    console.log('EMIT COMMAND', command, data);
                }
                return ds;
        });

    }


};