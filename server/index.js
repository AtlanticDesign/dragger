var AWS = require("aws-sdk");
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var crypto = require('crypto');
var path = require('path');

//var s3 = require('./s3');
var dbe = require('./db');

var dragTable = "draggers";
var visitsTable = "dragVisits";
var exCommand;

AWS.config.update({
    region: "us-west-2",
    endpoint: "https://dynamodb.us-west-2.amazonaws.com"
});

var dbdc = new AWS.DynamoDB.DocumentClient();

// var isAnimating = false;

function formatMsg(obj) {
    var tn = Date.now();
    var obx = {
            "ts": tn,
            "things": obj
        }
    return obx;
}

function addItemToDb(obj,table) {
    var params = {
        TableName: table,
        Item: obj
    };

    dbdc.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
        } else {

        }
    });
}

var ds;

 
app.use('/static', express.static(__dirname + '/../static'));


app.get('/', function(req, res) {
    res.sendfile('client/index.html');
});

var userCount = 0;
var totalUsers = 0;
var clients;


// DRAG APP


io.on('connection', function(socket) {
    totalUsers++;
    userCount++;

    console.log('a user connected, total users: ', totalUsers);
    //console.log(socket);
    var address = socket.handshake.address;
    console.log('New connection from ', address)    
    console.log('User Agent', socket.handshake.headers['user-agent']);

    socket.oldUserNames = [];
    socket.appUserName = 'The homie '+totalUsers;
    socket.moveCount = 0;

    socket.on('disconnect', function() {
        updateVisitor();
        totalUsers--;
        console.log(socket.appUserName + ' Just disconnected after ' + socket.moveCount + ' moves.');
        //io.emit('users', totalUsers);
    });


    socket.on('client info', function(msg){
        var tn = Date.now();
        socket.oldUserNames.push(socket.appUserName);
        socket.appUserName = msg.userName;
        console.log('got clients usrname', msg.userName);
        // ADD visit record
        var vo = {};
        vo.userName = socket.appUserName;
        vo.hadCookie = msg.hadCookie;
        vo.userAgent = socket.handshake.headers['user-agent'];
        vo.address = socket.handshake.address;
        vo.timeIn = tn;
        vo.concurentUsers = totalUsers;

        var visitId = 'visitor ' + userCount + ' -- ' + tn + ' -- ' + vo.userName;
        //var xmsg = formatMsg(vo);      
        var obx = {
            "vId": visitId,
            "things": vo
        }       
        addItemToDb(obx,visitsTable,dbdc);        
        // ASSIGN THE 'ts' or Item ID to the socket here, so we can update on disconnect!
        socket.dbId = visitId;
    });

    // socket.on('animate start', function(msg){
    //     console.log('animate start');
    //     isAnimating = true;
    // });

    // socket.on('animate end', function(msg){
    //     console.log('animate end');
    //     isAnimating = false;
    // });



    socket.on('client location', function(msg){
        socket.userLocation = msg;
        console.log('Got user location for '+ socket.appUserName, socket.userLocation, msg);
    });


    socket.on('CLIENT get comp', function(msg){
        var dbread = readDb(dragTable, 'SERVER send comp',dbdc, function(){});
        socket.emit('is animating', isAnimating);        
    });

    socket.on('drag end', function(msg){
        var xmsg = formatMsg(msg.posAll);  
        socket.moveCount++;    
        addItemToDb(xmsg,dragTable,dbdc);
        msg.appUserName = socket.appUserName;        
        socket.broadcast.emit('update drag', msg);
    });

    socket.on('drag start', function(msg){
        var msgObj = {};
        msgObj.shapeId = msg;
        msgObj.appUserName = socket.appUserName;
        console.log(socket.appUserName + ' is dragging ' + msg);   
        socket.broadcast.emit('flag drag', msgObj);
    });
    socket.on('drag live', function(msg){
        socket.broadcast.emit('draging live', msg);
    });


    function readDb(table, command) {

        var params = {
            TableName: table,
            Limit: 50,
            ConsistentRead: true
        };

        dbdc.scan(params, function(err, data) {
            if (err)
                console.log('DB scan error');
            else
                ds = data;
                if (command) {
                    socket.emit(command, data);
                }
                return ds;
        });

    }


    function updateVisitor() {
        var tn = Date.now();

        var params = {
            TableName: visitsTable,
            Key:{
                "vId": socket.dbId,
            },
            UpdateExpression: "set things.outTime = :t, things.moveCount = :m, things.userLocation = :l",
            ExpressionAttributeValues:{
                ":t": tn,
                ":m": socket.moveCount,
                ":l": socket.userLocation
            },
            ReturnValues:"UPDATED_NEW"
        };

        console.log("Updating the item..." + socket.dbId);
        dbdc.update(params, function(err, data) {
            if (err) {
                console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
            }
        });        
    }    

});



http.listen(6969, function() {
    console.log('listening on *:6969');  
});
