// INCLUDE GULP
var gulp = require('gulp');
//var exec =  require('child_process').exec;

// INCLUDE PLUGINS
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var compass = require('gulp-compass');
var plumber = require('gulp-plumber');
var jswrap = require('gulp-js-wrapper');


// LINT JS
gulp.task('lint', function() {
    return gulp.src('static/js-src/*.js')
        .pipe(jshint())
        .pipe(plumber())
        .pipe(jshint.reporter('default'))       
});


// COMPILE SASS
gulp.task('sass', function() {
  return gulp.src('static/sass/*.sass')
    .pipe(compass({
      config_file: 'config.rb',
      css: 'static/css',
      sass: 'static/sass'
    }))
    .on('error', function(error) {
      console.log('COMPASS ERROR');
      this.emit('end')
    })    
    .pipe(gulp.dest('static/css'))
});



// CONCATENATE & MINIFY JS
gulp.task('scripts', function() {
    return gulp.src([
          'static/js/setup.js',
          'static/js/vars.js',
          'static/js/createShapes.js',
          'static/js/dragEnd.js',
          'static/js/dragStart.js',
          'static/js/dragMove.js',
          'static/js/audio.js',
          'static/js/audioDrone.js',
          'static/js/audioArpeg.js',
          'static/js/socket.js',
          'static/js/animateComp.js',
          'static/js/commandments/*.js',
          'static/js/name/animals.js',
          'static/js/name/adj.js',
          'static/js/name.js',
          'static/js/util.js',
          'static/js/cookie.js',
          'static/js/animate.js',
          'static/js/messages.js',
          'static/js/location.js'
        ])
        .pipe(concat('dragger.max.js'))
        .pipe(plumber())
        .on('error', function(error) {
          // Would like to catch the error here 
          //console.log(error);
          //console.log(error.toString())
          console.log('SCRIPTS ERROR');
          this.emit('end')
        })           
        .pipe(jswrap({
            // pass a safe undefined into the encapsulation
            opener: 'function dragger_init(){',
            closer: '}'
        }))
        .pipe(gulp.dest('static'))
        .pipe(rename('dragger.min.js'))
        //.pipe(js_obfuscator())
        .pipe(uglify())
        .pipe(gulp.dest('static'));
});


// WATCH FILES FOR CHANGES

gulp.task('watch', function() {
  gulp.watch([
        'static/js/*.js',
        'static/js/**/*.js',

    ], gulp.series('lint', 'scripts'));
  gulp.watch([
        'static/sass/*.sass',
        'static/sass/**/*.sass',
    ], gulp.series('sass'));  
});


// DEFAULT TASK
gulp.task('default', gulp.series('lint','scripts','watch'));
